Studio Setup 2.0 - Cycles

VERY IMPORTANT LICENSE INFORMATION:

   This file has been released by RegusMartin under the following license:

Creative Commons Attribution 3.0

   You can use this model for any purposes according to the following
   conditions:

     * You MUST give attribution/credit to RegusMartin.
     __________________________________________________________________

Blend information

     * Name: Studio Setup 2.0 - Cycles
     * Author: RegusMartin.
     * Premalink: http://www.blendswap.com/blends/view/74897.
     * For Blender 2.52 and up.
     * Description:
       I created this lighting setup for displaying my own models in a
       consistent form. I hope that this is useful for anyone else that
       needs this. Happy blending!
       Update! v2.0
          + Improved reflections
          + Depth of Field Control
          + Updated Lighting

       Note: The scene included renders like the first preview image. The
       rest are made my changing the color of the backdrop and the
       lights.

