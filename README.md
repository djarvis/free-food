# Vertical Farm Energy Calculations

This page describes the energy calculations for a vertical farm powered by
renewable energy. Consider the following assumptions:

* an average adult requires 7,500 kJ per day (minimum);
* a densely populated area of 1,626,159 people;
* annual energy requirements of 4,451,610,262,500 kJ per area;
* an average city block is 18,769 m²;
* total area afforded to a one-block, 20-storey building is 375,380 m²;
* one storey is 3 meters;
* one third of the floor space is allocated to infrastructure;
* the building grows high-energy density food, with multiple racks per storey;
    - Chickpeas (686 kJ per 100g) -- six racks
* three growing seasons using chlorophyll-tuned LED illumination;
    - 303 lumen per Watt @ 350 mA
* solar cell efficiency is 46%; and
* the solar panel area is 10,000 m².

## Dwarf Avocado Plants

Types of Dwarf Avocado plants include: Gwen, Holiday, and Wurtz. Resources
for yield and dimensions include:

* [Four Winds Growers](https://www.fourwindsgrowers.com/fruit-trees-vines-and-berries/avocados.html)
* [Queensland Government](https://www.daff.qld.gov.au/plants/fruit-and-vegetables/fruit-and-nuts/avocados/harvesting-and-yields)
* [Backyard Avocados](http://www.backyardavocados.com/varieties-of-avocado-trees/)

### Yield Calculation

* Energy Density ≈ 1,840 kJ per fruit (300 g)
* Height ≈ 1.8 m
* Diameter ≈ 2.5 m
* Area ≈ (2.5 m / 2)² * π ≈ 4.91 m²
* Yield ≈ 21 fruit

Energy ≈ Yield * Energy Density
       ≈ 21 fruit * 1,840 kJ per fruit
       ≈ 38,640 kJ per plant


## Quinoa

* Diameter - 0.3556 m
* Height - 1 m
* Harvests - 4
* Unit Energy - 872 kJ
* Units per Plant - 1

## Potato

* Diameter - 0.3048 m
* Height - 0.3048 m
* Harvests - 4
* Unit Energy - 421.5 kJ
* Units per Plant - 1

## Watercress

* Height - 0.127 m
* Width - 0.076 m
* Harvests - 52
* Unit Energy - 1 kJ (?)
* Units per Plant - 1

## Dwarf Macadamia Plants

Resources for the Dwarf Macadamia plant yield and dimensions include:

* [Daleys Fruit](http://www.daleysfruit.com.au/buy/dwarf-macadamia-tree.htm)
* [Wikipedia](http://en.wikipedia.org/wiki/Macadamia)
* [Incredible Edibles](http://www.edible.co.nz/fruits.php?fruitid=69)

### Yield Calculation

The yield per plant is calculated using:

* Energy Density ≈ 2,960 kJ per nut (20 g)
* Height ≈ 1.5 m
* Diameter ≈ 2.2 m
* Area ≈ (2.2 m / 2)² * π ≈ 3.8 m²
* Yield ≈ 13 kg ≈ 13,000 grams ≈ 650 nuts

## Solar Power Calculations

Using the [PVWatts](http://pvwatts.nrel.gov/) calculator tool, the
following estimated energy input is attained from ultra-efficient solar
panels (put to use in Victoria, British Columbia):

```
Energy = Array Area (m²) × 1 kW/m² × Module Efficiency (%)
       = 10,000m² * 1 kW/m² * 46% = 4,600 kW
       = 5,182,905 kWh per year
```

### Results

The following table provides an estimated monthly energy production for the
solar panel array.

| Month       | Sunlight | Energy (kWh) |
|-------------|----------|--------------|
|   January   | 1.21     | 139611       |
|   February  | 2.26     | 237215       |
|   March     | 3.3      | 383824       |
|   April     | 4.66     | 520573       |
|   May       | 6.09     | 695794       |
|   June      | 5.7      | 631708       |
|   July      | 6.48     | 731924       |
|   August    | 6.16     | 697785       |
|   September | 4.67     | 512743       |
|   October   | 2.76     | 315921       |
|   November  | 1.69     | 187335       |
|   December  | 1.13     | 128469       |

## Additional Resources

* [Food Energy in kJ](http://apjcn.nhri.org.tw/server/info/books-phds/books/foodfacts/html/data/data2a.html)

# Question

The vertical farm energy calculator helps determine the size of a building necessary to produce a crop that can sustain the energy requirements of a local population. I would like help calculating whether it is possible to power the building to grow the food indoors using renewable solar energy.

The PVWatts calculator tool determined that the total energy output for a 10,000 sq. meter solar array in the rainy Pacific Northwest averages 5,182,905 kWh per year (e.g., using the south-facing side of the building, at 122 meters x 90 meters; the rooftop would also be possible, given its potential 122 m^2 area). This wattage is possible based on the December, 2014 announcement of a multi-junction solar panel that converts 46% of sunlight into electricity.

In early 2014, a company announced 303 lumen lights at a correlated color temperature of 5150 K and 350 mA. By tuning colour ratios, LED lights can activate specific areas of the spectrum necessary for photosynthesis. This allows otherwise unused wavelengths of light (collected by solar panels) to be re-emitted as photosynthesizable light (by wavelength-tuned LEDs). Chlorophyll A absorbs wavelengths of around 430 nm and 662 nm; Chlorophyll B maximally absorbs at 453 nm and 642 nm; Carotenoids absorb light maximally between 460 nm and 550 nm.

The calculations so far indicate that growing average energy-density food (e.g., potato, quinoa, watercress, pineapple) would take four buildings, each 265 m^2 x 90 m high, to supply the energy and nutrient needs of a population shy of 345,000 people.

Could sufficient solar energy be repurposed to photosynthesizable light for growing these crops indoors?

