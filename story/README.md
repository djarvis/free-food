# Build Scripts

The build scripts provide a way to build the books and overview.

# Book

Build scripts for the book.

## Full

Build the full novel as follows:

    ./build.sh -V -m -k -v -c

The document is built. The build script will output the number
of quotes that have been corrected. This number must not vary.

## Partial

After building the full novel, faster partial builds can be accomplished
by re-generating the PDF as follows:

    ./build.sh -V -c

The book is rebuilt using the same markdown documents produced from the
full run.

# Scenes

Scenes can be built in overview or summary mode.

## Overview

Build the overview as follows:

    cd overview
    ./build.sh
    cd ..
    ./build.sh -i overview -o /tmp/overview -f scenes

The full scene document is built.

## Summary

Build the short summary as follows:

    cd overview
    ./build.sh -s summary.xsl
    cd ..
    ./build.sh -s summary -i overview -o /tmp/overview -f summary

The scene summary document is built.

## Review

Build the review version of the summary as follows:

    cd overview
    ./build.sh -s markdown.xsl -o /tmp/overview
    cd ..
    ./manuscript

