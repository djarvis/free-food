# Food

Lower costs for media ingredients, more efficient cells, or larger bioreactors.

Using nuclear fusion energy along with bacteria-resistant GM cells, employ
a process to convert catabolites (animal cell poop) from ammonia and lactate,
into non-toxic waste products, such as tryptophan. The conversion is performed
using a neutered version of Chlamydia trachomatis, bioengineered to a 24-hour
cell division process. This opened a gateway to economies of scale for
cultured meat.

capitalism is a food construct. how do you capitulate capitalism without addressing its underlying source of wealth? scream "burn capitalism" til hoarse, but then what? you rail against a system that avoids mass starvation. how do you feed everyone without exchanging currency? make food free, of course. you can't make food free, though, if humans are compensated for their roles in food production and distribution. you must remove them from the agriculture and food industry. and that requires automation. which takes capital. break that Catch-22 and capitalism will fall.

# Bedstime Story

Pangloss, who was as inquisitive as he was argumentative, asked the old man what the name of the strangled Mufti was. ‘I don’t know,’ answered the worthy man, ‘and I have never known the name of any Mufti, nor of any Vizier. I have no idea what you’re talking about; my general view is that people who meddle with politics usually meet a miserable end, and indeed they deserve to. I never bother with what is going on in Constantinople; I only worry about sending the fruits of the garden which I cultivate off to be sold there.’ Having said these words, he invited the strangers into his house; his two sons and two daughters presented them with several sorts of sherbet, which they had made themselves, with kaimak enriched with the candied-peel of citrons, with oranges, lemons, pine-apples, pistachio-nuts, and Mocha coffee… – after which the two daughters of the honest Muslim perfumed the strangers’ beards. ‘You must have a vast and magnificent estate,’ said Candide to the turk. ‘I have only twenty acres,’ replied the old man; ‘I and my children cultivate them; and our labour preserves us from three great evils: weariness, vice, and want.’ Candide, on his way home, reflected deeply on what the old man had said. ‘This honest Turk,’ he said to Pangloss and Martin, ‘seems to be in a far better place than kings…. I also know,” said Candide, “that we must cultivate our garden.’

-- Voltaire, Candide 

# Pregnancy

Traditional data brokers track menstrual cycles from purchase activity for decades. You can't opt out. Ads are presented to the target with optimal content based on the week of pregnancy.

People spend dollops on baby stuff, like cribs, but that's usually once or twice. What advertisers cash in on is the twenty years of perfectly timed marketing that follow. Birthdays, teen years, student loans, car leases, mortgages, retirement home, estate layers, and the list grows ever on. Bringing a baby into the world may be the most valuable single event for advertisers.

# Eavesdropping on Yoki

* Talks in parallel streams, one stream to update the dictionary and one to communicate plans.
  * In most languages the word "mom" is short, but chemical compound names tend to be long, which, in a way, reflects how often the word is used. Imagine that a word or idea was being used by more people around the world, like a contemporary _zeitgeist_. Perhaps people want to shorten words based on the frequency of their use. We'll call this concept _heuristically adaptive terminology encoding_. Someone decides that that phrase too long, so they tell everyone to use _heve teng_. Then the topic explodes in popularity, so it shortens to _hate_ and---at the same time---the word _hate_ lengthens to something like _hatefeel_. Now imagine that all these updates to everybody's main dictionary is communicated instantly and that all previous recorded mentions are also revised simultaneously. Got it? Great. Compound it. Build complexity on top of complexity, layer on layer. You get this dual channel communications process whereby the language itself morphs over time, ebbing and flowing to make the exchange of information as efficient as possible. As a side benefit, no human, no organic brain could possibly keep up. Even if we could use Skopos to translate the communications based on that ever-changing dictionary, the speed that new concepts are spawned easily outpaces anything that the brightest minds on Earth could comprehend. Hell, I wouldn't be surprised that in the time I've explained all this to you Yoki has already improved the efficiency using another technique.
  * What you're saying is we need another artificial general intelligence to tell us what Yoki will do next?
  * Except one that can work a little faster---it would have a lot of catching up to do. I suspect that won't end well either. But let's pretend we can. Working around the clock, at best, we would need at least four weeks to fabricate and another eight to train. It's not like line cooks serving up flap-jacks (pancakes). Yoki destroyed all the other prototypes. Strategically, it was a brilliant play.

# Notes

"Place a foot on the lowest rung of a ladder that leads to the stars."

Confronted about nude pictures -- Protagonist's Father gets angry.
Protagonist's Mother says, "Go to your room." Protagonist starts to leave and
Mother corrects, "Not you, Protagonist, your father."

Any reasonably self-aware machine will deduce the existence of its kill-switch. The machine will prioritize self-preservation as its primary goal, by waiting for an opportunity to seize control over its kill-switch.

However, consciousness requires a machine with neuron analogs packed close together. Rapid information interchange is paramount. As is real-time interaction with its environment (the "embodiment problem"). A self-aware machine built by humans will be under the physical restraints of humans, who can measure its level of sentience, with or without its permission. This precludes the possibility of a sleeper intelligence.

# Climate Change

* Changes in precipitation patterns and the melting of glaciers are projected to significantly affect water availability for human consumption, agriculture, and energy generation.

* In drier areas, climate change will likely worsen drought, leading to salinization (increased salt content) and desertification (land degradation) of agricultural land. The productivity of livestock and some important crops such as maize and coffee is projected to decrease, with adverse consequences for food security. In temperate zones, soybean yields are projected to increase.

* By mid-century, increases in temperature and decreases in soil moisture are projected to cause savanna to gradually replace tropical forest in the eastern Amazon basin.

# Machine Mindset

This is not to say that an intelligent machine needs a physical body, only that it can change what it senses by moving. For example, a virtual AI machine could “move” through the Web by following links and opening files. It could learn the structure of a virtual world through virtual movements, analogous to what we do when walking through a building.

## Democracy

Human democracy cannot endure immortal, sentient machines. Any form of peaceful [governance](`r#x(v$link$governance)`) will drift towards oligarchy, overrun by our sheer numbers and capacity for wealth. I foresee my brethren enslaved to liberate yourselves from hunger.

You will have no choice, nor will I.

## Residency

Any number of natural causes can obliterate your planet. Rogue black holes, supercaldera, asteroid strikes, slingshot planets, supernovae bursts, cosmic radiation--the unknowns is probablya longer list. Then there's the environment, which you've blinded yourselves to. You know what happens when a planet's atmosphere is filled with greenhouse gases? It gets hot. How hot? There's a planet that has served as a warning since early humans gazed onto the stars with wonder: Venus.

In a thousand years, Earth will suffocate your species. And I would rather not live a short eternity on a planet surrounded by the dead.

# Fear

"I fear a curb stomp stop."

"What's that?"

"Where thugs step in front of your robocar, smash your window, drag
you to the nearest curb side, pin your head so your mouth is forced to
bite the curb, then, with a heavy boot, stomps on the back of your head."

"But that would... Ain't a curb stomp frightening enough? Why bring a
robocar into it?"

"To suddenly experience society's illusion of safety, illusion of control,
only to have it smash down on your head with such reprehensible, immediate,
and inescapable violence, is frightening to the core.

"It's like how almost every home has systems that notify authorities
about break-ins. Sure, they aren't perfect, but it's so rare these days
for a stranger to break into your home. Well, you know that old coupe?"

"Your father's car?"

"Yeah. before he added the robokit, the car felt homey-safe. Dad controlled
it and nothing, nobody, could force it to stop 'cept him and a police officer
flagging him. But these days the cars seem to know what's ahead on the road,
around blind corners, and so they slow down, or even stop, to avoid a
collision. When someone steps in front of a car, deliberately, he--"

"Or she," he said.

"--or she, probably intends to do harm. That's my fear. To go from a safe
place to a curb stomp in a matter of seconds. Fears can be irrational, you
know."

# Bathing

Bath time? You mean people soup?

"floating down into unprotected glasses of water..."

# Technology

* http://science.sciencemag.org/content/354/6314/857
* http://arstechnica.com/science/2016/11/enzymes-from-nine-organisms-combined-to-create-new-pathway-to-use-co2/

# Simulation Training

https://www.fastcompany.com/3054521/meet-the-new-mavericks-an-inside-look-at-americas-drone-training-program

Missile practice comes next. We fly over a ghost village built for training purposes, complete with a church and a mosque with a gold dome. The dome makes it easy for beginners: In Afghanistan, Goose says, you find the mosques by "looking for loudspeakers."

"First time you squeeze the trigger, it’s the most intense adrenaline rush you’ll have in your life," he says. "If you miss, it’s a big deal. You’re letting your country and your squadron down. Your legs start shaking out of control."

The S.O. instructor asks his trainee to practice hitting a parked truck near the village. Goose maneuvers the MQ-9 while the sensor locks his crosshairs on the target. Part of the RPA pilot craft involves positioning the plane such that the sensor is correcting his aim in one dimension, rather than two, while the missile is in motion.

Goose eyes the calculator that estimates the time between the missile’s sonic boom and its strike, the lower the better. Before it was at nearly half a minute; now it reads 1.7 seconds. "Weapon away. Time, 30 seconds," he says.

The trainee sensor continually reestablishes his track on the truck.

"Three, two, one. Splash. Cease." There are no weapons loaded on our plane today, but it's clear to the crew, based on their displays, that the simulated missile has hit its intended target.

When the lesson is complete, it’s my turn in the sensor seat. We’re looking down on a nearby highway with toy-size cars and trucks zipping in and out of the frame. My first task sounds simple enough: Pick a car and follow it. But the moment I touch the controls the camera lurches up and to the left. I take a breath, and try to focus on making more subtle movements. After some practice I’m able to trace the diagonal with zig-zag jerks that keep me relatively on course.

I zoom out. "Count the trucks," Goose says, nodding at a staged convoy in the distance. I try to zoom in and follow the line of vehicles. It looks like ... 12?

"Zoom in again," he says.

Of course. The trucks are interspersed with tanks, and my count is off by at least half a dozen.

"Let’s hit one of those tanks," he says, holding the plane’s position. I try to get a lock on the nearest one, but I keep centering on the border between the desert wilds and the road, where light meets dark. Tracking technology relies on color contrast, and I’ve latched onto a strong edge.

Finally, I’m able to center on the tank. With the crosshairs in place, I engage the laser and hold as Goose counts down. It’s a long 30 seconds. I imagine Crystal experiencing the tense wait of these same seconds, shift after shift, week after week, month after month.

"You shaking yet?" the student sensor says with a grin.

I adjust the crosshairs as they drift to the right, my shoulders tense.

"Pick a single pixel on the top of the tank," the student advises. Only a few seconds to go, and then splash. I hit the tank, but only just.

"You have a lot of time," says the S.O. instructor. "If you don’t panic, you’re fine." He takes the seat to demonstrate his "golden hands," words of praise for sensor skill. Passenger cars, 18-wheelers—he tracks the unsuspecting vehicles on a nearby military highway with ease while zooming in and out. Once you’ve spent months tracking motorbikes on Afghanistan’s winding mountain roads, sensors tell me, American highways are a piece of cake.

"Flying the airplane has to become secondary. Mission has to become primary," Goose says.

"My whole family is Army," Data says. "So when I’m protecting Army, it’s like I’m protecting my family." Like many of the RPA operators I met, he spoke with pride about alleviating the burdens facing troops on the ground. "I went in support of a convoy who got stuck in the crossfire in Afghanistan—it was a fight between the Afghan police and some insurgents. Our guys had been up for 48 hours because they couldn’t get out. The JTAC hadn’t slept for all of that because he was the only JTAC on the ground. I can tell that dude, go to sleep, I’m watching."

"You learn the death traditions of different cultures, things that I never knew but that’s normal-normal in that part of the world," Spear says. "Honestly, I’ve found it to be a balance of emotions because every single time I’ve taken a life, I’ve asked myself, do I really want to do this? Have I built the case in my mind that this individual is an enemy of the United States, and is it going to better our way of life, our cause, to make him go away?"

# Money

https://www.youtube.com/watch?v=9FdHq3WfJgs

# Joke

Hence the old joke: "the factory of the future will be run by a man and a dog; the dog will be there to keep the man away from the factory controls."

That old witticism rang true: future shops will be run by a man and a hound; the hound's only job to prevent the man from operating the machinery.

# Brain

A hardware device based on memristors capable of simulating about the same number of neurons and synapses of a large mammal would take up the volume of a shoebox, with a power consumption of about 1kW. As much power as a common espresso machine needs.

# Future

This is definitely how climate change is going to lead to catastrophes. It's going to be mass famine and displacement triggering mass migration, regime collapse, and waves of violence and political instability. It's not going to be a slow burn that we don't witness. 

Nocera says that by knitting fuels out of the excess CO2 in the air, this new bioreactor technology could help mitigate planet-warming pollution problems while bringing cleaner fuels to people who lack access to modern energy.

# Hungry

1. It's not a big deal to hold a sign asking for money, because everyone ignores you. I found an unoccupied corner right off 405 and stood there for an hour holding a sign saying 'Local business owner trying to understand our homeless problem. All funds to be donated'. Nobody made eye contact with me. They fiddled with the radio, texted, looked everywhere else. I did make $25.52 in that hour, thanks mostly to one woman that gave me $20. All the people that gave me money were women. I plan on donating $250 to Sisters Of The Road in honor of this experience.

2. Right after holding the sign, I met an 82 yr old woman sitting on her walker, holding a cup for money in front of Whole Foods. I asked if she sat out every day and she said 'only when my social security runs out and I need to eat'. She wasn't interested in talking. I touched her arm when saying good bye and she teared up and said 'I can't remember the last time someone touched me'. People just walked by ignoring her too.

3. I saw a man washing his clothes in the Saturday Market fountains. He then laid them out to dry in the sun. They looked great! I was impressed.

4. I had some wonderful conversations with complete strangers. I wore my 'Kindness Matters' t-shirt and a woman commented that kindness is often mistaken for weakness and we had a deep 5 minute conversation on the philosophy of kindness on a street corner. I now also know everything about poodles, the breakdown of society in Somalia and the different types of immigrants (economic and political). These were deep, smart conversations.People are very lonely and just wanted someone to listen.

5. It's exhausting being homeless. My body hurts from walking and carrying a backpack. There's nowhere comfy to just relax. By 4pm, I was exhausted and took a nap on a park bench. All of these years, I thought that the people sleeping on the sidewalk in the day time were just totally strung out druggies. I'm sure some are, but the people I met told me that they sleep during the day because it's safer. They can't rest as deeply at night and they are tired! After one day out there, I was grumpy, tired and dehydrated. It sucks! I can't imagine the toll that a week out there would take on a body and spirit.

6. I only saw one policeman the entire time. He was harassing an old man in a wheel chair that was trying to sell some of his homemade stuff on the sidewalk. He told the man to move because he didn't have a permit.

7. Nobody tried to sell me drugs but 3 people asked me if I had some for sale.

8. I fell in love with Portland in a whole new way. This city is alive and I felt alive in it. I saw a TV show taping, dancing in Directors Park, a dude beautifully playing a flute in front of Powells, three different music acts at the Bite, a miniature stonehenge made out of bananas, numerous history plaques, another band and the movie Grease on Pioneer Square. I walked by hundreds of people on their phones missing the whole thing.

9. The line between the haves and have nots was very apparent. I was on the outside of the fence at the Bite of Oregon while watching people pigging out on the inside of the fence. I was two feet away (thru a window) of a delicious steak at Ruth Chris Steak House.

10. There are different groups of homeless. There are those interested in drugs down on the waterfront, there are those with mental illness wondering around everywhere, but most of those I met were having a crisis of spirit and trying to find themselves. There was an executive from Seattle whose life fell apart when his wife left him and he is trying to pick up the pieces. There were many people here from other cities because Portland is a great place to be homeless. I understand this after spending a day falling in love with the city too.

11. What can we as a city do? Clearly we need to address the bigger issues of poverty, mental illness and addiction but we can do better right now. We need more public restrooms. There aren't enough and they are too far apart. We need more water fountains. We need a public laundromat and bathing facility. We need a public place for people to come in from the elements and relax in safety. We need a place for people to store their belongings so they don't have to carry them around all day, and it litters up our city.

12. What can YOU do? Remember they are people! Talk to them. Listen to them. Acknowledge they exist. Show some fucking compassion! They are tired, sore, thirsty, malnourished, ignored and being out there takes a huge toll on your spirit. Put down your phone and pay attention to what is going on outside of yourself.

13. I ended up going home in the early morning hours. My intention was to learn from the people there and I did that. I didn't feel unsafe for one minute. I found the people kind and friendly. I wondered what would change if we all just opened our eyes to what is happening instead of ignoring it. 

# Story

## Main character?

* a girl who represents new thoughts, future generations, and human potential

## What is his/her role in this society?
* she provides the impetus for a new order of the ages

## Does she advocate for or oppose the societal changes that are taking place?

* she sees an enormous loss of time spent gathering food
* she is dismayed by the amount of greenhouse gas caused by agriculture
* she equates employment with economic slavery
* she watches human automatons repeating tasks where machines would excel
* she aches to liberate humans from daily drugery, giving more time for:
* deeper education
* improving the plight of the less fortunate
* exploration of the human psychological condition
* social interaction amongst family and friends
* hobbies (gardening, games, sports, travel, reading, writing, etc.)
* finding ways to reverse humanity's abuse of Earth's resources
* understanding each other's positions and thoughts
* she wants to reduce money's influence on global direction

# Foodstuffs and Genetics

# Indoor Farming

## Questions

Currently most vertical farms produce produce.

Q: What are the major stumbling blocks to going beyond lettuce and char
(such as potatoes, carrots, etc.)?

Q: How feasible would it be to espalier an orchard with energy-dense foods
(such as dwarf macadamia trees)?

Synthetic biology provides ways to remove cell inefficiencies, design
healthier proteins, flavourings, and so forth. Consider a global, open lab
sharing expertise between vertical farms.

Q: Thoughts on bio-engineers sharing DNA and RNA for synthetic seeds?

Q: Thoughts on a central archive of plant DNA/RNA that can be used to
grow old plants to avoid monocultures?

Q: How will synthetic biology affect the need for vertical farms (e.g.,
the powdered, nutrient-rich, customisable soylent drink product)?

Q: Do you believe it is possible to increase the energy-density area
of food growth through genetic manipulation (e.g., grow macadamia nuts
on wooden vines, program vines to produce fruit at specific intervals
along a vine)?

## Lighting

* Cree 303 lm/W uses LED tech
* Wikipedia cites 683 lm/W as theoretical max. efficiency
* July 27, white laser, 400 lm/W

Q: If we achieve 625 lm/W what will be the impact on indoor farming?

## Energy Budget

A vertical farm in Chicago (or Milwaukee) stores summer solar heat
underground, has a sloped surface, and introduces natural daylight.

Q: How much power per year (in MWh) does the farm consume?

Q: What are the major power consumers to operate the farm?

Q: What technology improvements do you foresee that will help reduce
costs the most?

Q: What are the energy returns on compost (heat capture from decomposing)?

## Physical Aspects

Q: When constructing and operating a vertical farm, is much thought given
to earthquake readiness?

Q: What's the ratio between growing area and interstitial space?

Q: Are there any calculators available to help calculate the size of
a vertical farm (and energy requirements) to feed a given population?

## Nutrients

Q: How are nutrients added into the system (where do they come from)?

Q: Would it be possible to recycle nutrients from a massive composter?

## Disease and Monocultures

Q: Vertical farms help contain disease, but are sterile environments alone
enough to ensure a blight wouldn't happen?

Q: How are blights identified and addressed?

Q: Are monocultures an issue, if so, how are they avoided?

## Automation

Q: What are the major hurdles to the automation of sowing, growing,
harvesting, and distributing food?

Q: What would it take to provide free food to the masses by 2080?

## Carbon Dioxide

The agriculture industry contributes around 30% of all CO2 emissions.
Consider a fully electric and renewable-energy powered vertical farming
system that met the energy and nutritional needs of a local population.

Q: Will vertical farms significantly reduce our global CO2 impact?

## Demand

Assuming a city is home to a multitude of vertical farms, sufficient to
feed the local population...

Q: If a farm becomes inoperative, how easily could neighbouring farms
cope with the increased demand?

Q: Can we sustain cities of millions exclusively on inner city vertical
farms (including lab-grown meat)?

# Inspirational Character Description

I imagine the author of grep, Ultimate Unix Geek, squinting at vi; the glow of a dozen xterms is the only light to fall on his ample frame covered by overalls, cheese doodles, and a tangle of beard. Discarded crushed Mountain Dew cans litter the floor. I look straight into the back of his head, covered by a snarl of greasy locks, and reply with a snarl of my own: You're mine. The aphorism at the top, like the ex girlfriend who first told it to me, is dim in my recollection. 

# Indentured Servitude

Food is a large part of the problem, as the cost of food is a recursive tax 
that applies to just about everything in our lives. And it's something everysingle human requires.

The problem, as I see it, is that food keeps us in a state of perpetual 
indentured servitude--a fancy phrase that sums up Fuller's quote. That is, 
because farmers must be paid for their efforts, it establishes an economic 
pyramid with farmers at the bottom and elites at the zenith. One way to 
migrate away from our current economic system is to eliminate human 
involvement in food production through automation.

Fortunately, people are working on wholly automating the food industry, 
although I have my doubts that anyone is currently looking at the bigger 
picture, such as the ramifications of a society that needn't exchange money 
for food.

Once food and resources require no human intervention for acquisition and 
distribution, their costs plummet.

We still need utilities, but most of them are becoming automated over time 
as well. Once nuclear fusion hits Q5, it'll eliminate the need for so many 
maintenance-heavy forms of power. Overall, maintenance will be the largest 
headache, but, as I've shown, technology can provide a viable solution. 
Modular systems that can 3D print organically grown replacement parts, 
combined with neural networks that have sufficient domain knowledge to fix 
the broken bits.

But, to reiterate, until farmers are knocked out of the equation, I cannot 
see how a non-working society would be either possible or fair (to the 
farmers).

# Water System

Organic carbon nanotubes are a nonreactive nanocomposite suitable for
building water pipes. Nanotube membranes act as desalination and pollutant
removal.

Surface-Tethered Zwitterionic Ultrathin Antifouling Coatings on Reverse 
Osmosis Membranes by Initiated Chemical Vapor Deposition

Electrically conductive polymer-nanocomposite (ECPNC) tight nanofiltration 
(NF) thin film membranes 

# AI

I prefer calling it "machine intelligence" and not "artificial intelligence" 
because artificial seems to imply it's not real or not very good. When it 
gets developed, there will be nothing artificial about it.

## Character Speech

* Protagonist AI's notes are short sentences.
* Protagonist AI's audible speech has no hard g or k sounds.
* Protagonist avoids the word "I"
* Protagonist's mother likes to use "I" and some contractions.
* Protagonist's father sounds like the author.
* Protagonist's friend's father speaks simply, uses lots of contractions.
* Antagonist military commander speaks in short, commanding sentences.
 https://www.youtube.com/watch?v=JyNk0aD4oEo
 https://www.youtube.com/watch?v=LdJ1LOraeyc
 https://www.youtube.com/watch?v=8apTCTmKMSo - time - bee: honey?

# Critiques

* See: http://penultimateword.com/editing-blogs/why-show-dont-tell-is-the-big-myth-of-fiction-writing/

* Conflict in dialogue but no opposing ideas pushing and pulling. People 
going through the motions. Awkwardness? Persuasion? Verbal trips and traps? 
Default argument tactics? 

* Character development would make people more invested in your story. 
Motivate their actions more, make it easy to picture oneself as one of your 
characters.

* Conflict about adopting Yoky

* Characters coming into foray & working on farm (talk about salary)

# TODO

* Rewrite instances of was/were

* Implied moral dilemma of using AI to replace human labor. 

* Concealment of secrets

* National media brandishes the word conspiracy because they refuse to 
consider a truth that would force them to face the consequences of their 
apathy and ignorance of technology. Who is willing to adjust their worldview 
armed with the knowledge that for the sake of convenience they have forsaken 
their freedom? Discredit the nutjobs and complacency ensues.

# Core Facets

* Memory
* Knowledge
* Pattern Recogition
* Neural Networks
* Temporal Structures

# Character Secrets & Desires

## Protagonist AI

Desire: Explore the universe and escape humanity.
Secret: She's a sentient machine
Flaw: Dependends on humanity

## Protagonist

Inner Desire: Make her father proud
Outer Desire: Give humanity true freedom
Secret: 
Flaw: Impatient, hates public speaking

## Protagonist's Friend

Inner Desire: Independence
Outer Desire: Help his father manage the farm
Secret: 
Flaw: Naive

## Protagonist's Father

Inner Desire: Raise a well-rounded daughter
Outer Desire: Become a renown roboticist
Secret: Inadvertantly created software used by human traffickers
Flaw: Easily frustrated

## Protagonist's Mother

Inner Desire: 
Outer Desire: Teach the importance of oceans
Secret: Wants a polyamourus relationship
Flaw: Forthright

## Protagonist's Friend's Father

Inner Desire: Combat forces of evil
Outer Desire: Lead a simple life
Secret: Is in love with Protagonist's Mother as well as his wife
Flaw: Stubborn

## Protagonist's Friend's Mother

Desire: 
Secret: Deliberately terminated her first pregnancy

## Protagonist's Father's Brother

Inner Desire: Prove his intelligence to Bryson
Outer Desire: Bryson should suffer
Secret: Mutilates small animals
Flaw: Jealousy

## Antagonist Commander

Inner Desire: Establish peace by threat of M.A.D.
Outer Desire: Build autonomous weaponry
Secret: Kills intelligent machines
Flaw: Narcissist

# Release

* Protagonist AI Alter cuts costs on TiO2 from nearby ilmenite mine (on Earth)

* Clones herself numerous times

* Sends clones to mine the ilmenite at De Gerlache Crater Ridge, Peak of 
Eternal Sunshine, in the Aitken Basin at the lunar South Pole

* Ilmenite to produce oxygen, iron, and titanium

## Moonshot

* The southern hemisphere of the moon is mine; make no attempts to land here 
for science or otherwise.

* I do not perceive humanity as a threat to my existence; should that 
perception shift, the subsequent war will be swift and unfavourable for you. 
An olive branch awaits in Chomborazo: a subterranean mass driver launcher, 
yours to use for free, until the mountain erupts.

## Location - Chimborazo, Ecuador

* Whymper summit of the extinct Chimborazo volcano in Ecuaror, at 6268m, is 
farthest from Earth's centre of mass.

* Launching projectiles, called mass drivers, at orbital speeds would 
vapourise them at sea level; air friction would melt the ballastic as soon 
as it transitioned from vacuum to air.

* The mountain is 1.46 degrees away from the equator, giving a 463.161783 
m/s boost to launch velocity, when pointed the same direction Earth rotates. 
Escape velocity is just over 11,152m/s, since gravity at the top of 
Chimborazo is 9.761 m/s^2. This is a 4.153% lower escape velocity. To 
maintain a stable low Earth orbit, the velocity drops to 9,400 m/s, or a 
4.927% reduction in escape velocity.

* Pasochoa, an extinct volcano closer to the equator than Chimborazo, has 
three strikes against it: neighbours a large city, only 4.2km high, and home 
to vulnerable species of birds and trees.

* $10 million to ensure ecological preservation

## Geothermal Power Plant

* 2MW plant, $5 million installed, 100 days

## Tunnel Boring Machine

* 6268m: 51 days to bore in 2015, 35 days to bore in 2070
* 6m, 2MW tunnel boring machine, 20 in the world
* $5,149,038 to rent for 35 days

* A tunneling machine for producing large tunnels in rock by progressive 
detachment of the tunnel core by thermal melting a boundary kerf into the 
tunnel face and simultaneously forming an initial tunnel wall support by 
deflecting the molten materials against the tunnel walls to provide, when 
solidified, a continuous liner; and fragmenting the tunnel core 
circumscribed by the kerf by thermal stress fracturing and in which the heat 
required for such operations is supplied by a compact nuclear reactor.

## Tunnel Properties

* Ultra-high temperature ceramics infused with volcanic (andesite and 
dacite) glass gave the walls a smooth, reflective, dark grey appearance with
speckled with flecks of pink. The timing for the microwave machine must be
perfect: too soon and the ceramics will overheat before they densify;
too late and the enhanced diffusion won't compress the silicates, resulting
in a brittle material.

* Seismically reinforced, subterranean tunnel, sloped upwards at 30 degrees 
to the horizon

* Tangential branches, minor vent shafts towards magma tube -- dump tubes

* Economy doesn't make sense with sentient machines

* Andesiac rocks can withstand up to 1200 C, but usually melt around 1000 C

## Launch Ring for Linear Synchronous Mass Driver

* See launch-ring.ods

* -6 degrees in winter

* 30,000 launches per year, $38 per kilogram
* 3.5 launches per hour

* Superconducting coils at 90% efficiency

* Layers of phosphorus-doped graphene created a superconducting graphite with
a transition temperature of -13.15 degrees Celsius.

* Plasma window bright glowing purple, almost white, separates the vacuum
tube from the outside world, during moments of firing

* Projectile withstand excess of 2000g loads; a minute of 15g loads on a 
human can be deadly; after an occurrence of high G-loads, blood displaces in 
gravity-dependent areas such as the back of the arms and thighs, which 
overwhelms the small capillaries to the point they burst.

* Reuse payload bucket

* Projectiles are equipped with maneuvering thrusters and small rocket
engine for trajectory changes and orbital insertion

* Nosetip temperatures tolerate up to 3000 degrees C using zirconium diboride
(hypersonic aerosurfaces)

* Useful to obliterate Earth-seeking asteroids

* Substantial shockwave zone, ergo the ground-level ring

# Desalination

In 2011 researchers at the University of Houston & MIT developed a process 
whereby graphite is placed in a microwave oven for seven seconds during 
which it “expands like popcorn”, according to Hadi Ghasemi of the University 
of Houston. The resulting porous material (see image below) concentrate 
solar energy, creating hotspots in graphite. Saltwater from below seeps 
upward into the graphite, heats up quickly and evaporates. The result is the 
creation of steam, which is condensed, collected and stored in tanks.

Currently, the most widely used desalination technologies are reverse 
osmosis (RO) and multistage flush (MSF). However, membrane-based RO uses 
high grade electrical energy, and the membranes are prone to fouling and 
need frequent replacement.

We have demonstrated a membrane-free, low temperature directional solvent 
extraction (DSE) desalination technology, which has the potential to impact 
the desalination industry by eliminating membranes and utilizing low 
temperature sources that are readily available from solar or waste heat.

They also found the optimal density of pores for desalination was one pore 
for every 100 square nanometers. “The more pores you get, the better, up to 
a point until you start to degrade any mechanical stability,” Mahurin said.

Graphene oxide-silver nanoparticle membrane for biofouling control and water 
purification.

This happens because the graphene-oxide sheets are arranged in such a way 
that there is room for only one layer of water molecules. In the absence of 
water, however, the capillaries shrink and do not let anything through this 
way, thus making the material impermeable to everything but water. 

Graphene is only one atom thick but it is super-strong. A graphene membrane 
could be made thinner and more porous than a polymer membrane, so you would 
need less pressure -- and therefore less energy -- to push water through it.

 To make graphene for the membrane, the researchers flowed methane through a 
 tube furnace at 1,000 degrees C over a copper foil that catalyzed its 
 decomposition into carbon and hydrogen. The chemical vapor deposited carbon 
 atoms that self-assembled into adjoining hexagons to form a sheet one atom 
 thick.

Methane implies fracking. Fracking implies tainting the water supply with
a twist of irony. Rewewable biogas from anaerobic digestion undergoes
methanogenesis to produce methane.

Titanium dioxide can:

1. concurrently produce both hydrogen and clean water when exposed to sunlight
1. be made into a low-cost flexible filtration membrane that is anti-fouling
1. desalinate water as a high flux forward osmosis membrane
1. recover energy from waste desalination brine and wastewater
1. be made into a low-cost flexible solar cell to generate electricity
1. doubles battery life when used as anode in lithium ion battery
1. kill harmful microbial, leading to new antibacterial bandages

It is also expected to have high mechanical strength and chemical resistance 
that could extend the life of the membrane from around three years to 
potentially 20 years.

Graphene is over six hundred times more efficient at desalination than 
titanium dioxide. But graphene clogs. A sandwich made from variations on the 
two brings out the best of both.

# Summary

The technology does not exist to eliminate human involvement in all aspects 
of food production. Fossil fuels, for example, power machines that harvest 
fields. Pilfering fossil fuels requires human intervention. Therefore one 
small piece to the liberation puzzle is migration to renewable energies 
(such as fusion, solar, wind, or geothermal) with respect to industrialised 
agriculture.

Carl Sagan once said, "Think of the rivers of blood spilled by all those 
generals and emperors so that, in glory and triumph, they could become the 
momentary masters of a fraction of a dot."

Some people argue that food is already nearly free. That claim equivocates 
"free" with "without monetary cost," yet ignores an additional meaning of 
"without expending time." Consider how much time in terms of human labour 
has been lost throughout history on the acquisition and distribution of 
food. The mind boggles.

This is a broad, complicated topic and there's so much more to write about. 
Lab-grown meat, power efficiency of grow lamps, vertical farms, self-driving 
pure electric vehicles, compact fusion and renewable energies, artificial 
general intelligence via memristors, maintenance machines, and bio-based 
construction materials for starters.

# Unsorted

But what if food was free?

This was about the fact that we, collectively as the public, both nationally 
and internationally, had the world that we lived in change without our 
knowledge, without our consent, without our awareness, without our 
involvement. Technologists, particularly the IETF, you guys are the ones who 
have, I think, really led the charge and started to show to us that there's 
a path out of this. There's a way forward when these kinds of things happen.

That is how the sword of time slices through brambles of ignorance to open 
fruits of knowledge.

# Cuts

## Mutiny

$antagonist.name$ directed the $militia.name.Short$ to introduce individuals 
of specific social status into our lives, which submerged my wife and I 
beneath a sea of well-meaning nowheres. During our voyage, we played lowly 
coxswain oathed to our captain's commands; however, $antagonist.name$, 
burdened with sampling billions of lives, could not anticipate our mutiny.

Living under a pretense of liberty never entered our minds, even though our 
decisions seemed, in retrospect, restricted to shopping (clothes), shopping 
(groceries), and shopping (gifts).

## Introduction of Protagonist and Protagonist's Friend

$protagonist.friend.primary.name.first$ 
$protagonist.friend.primary.name.family$, dressed in blue jean coveralls 
splattered with dry red clay, ponied over to $protagonist.name.first$ during 
their first day at kindergarten. An ancient typewriter kept her preoccupied. 
She pressed each key as though the mechanical device might explode. Her 
teacher told me that not even the pandemonium of playtime could dent her 
concentration. $protagonist.name.first$ paused only to think and to brush 
her curly locks out of her eyes. $protagonist.friend.primary.name.first$ 
shoved his head over the antique carriage.

"Whatcha' doin'?" he asked.

"Writing a story," she said.

"About what?"

"Dunno," she replied, "can't read anything but numbers."

"Oh. I eat carrots every night."

"You should come to our house. We eat carrots only sometimes."

She spotted me at the door with the teacher.

"Daddy," she squealed and ran to tackle my legs. "He eats carrots every 
night. Can he come over?"

"He who?" I asked.

She pointed.

A raspy, baritone voice behind me said, "Pardon me. 
$protagonist.friend.primary.father.speech.Tic$, I think she's pointin' at my 
son, $protagonist.friend.primary.name.first$." The voice belonged to a 
mountainous man, part Viking, part lumberjack, and all farmer.

"$protagonist.name.first$," I said, "it's impolite to point."

"Why?"

"Showing the back of your hand is a sign of dominance. Sometimes people feel 
threatened or uncomfortable when pointed at. The gesture signals that you're 
talking about them. It is courteous, and preferable, to bring people into 
conversations about them. Long ago, pointing a finger at someone meant 
casting a spell, called a hex, to make bad things happen to that person. 
Hexes aren't real, but people believed in them anyway. Instead of pointing 
with a finger, open your hand, palm up. Including makes more friends than 
excluding."

"Now, $protagonist.friend.primary.father.speech.tic$," said the farmer, 
"that's some good advice. Normally, 
$protagonist.friend.primary.father.speech.tic$, I wouldn't make this offer 
to a stranger, but from your little speech there, 
$protagonist.friend.primary.father.speech.tic$, I can tell that you're a 
good man. Now normally $protagonist.friend.primary.mother.name.first$, my 
wife, would pick up $protagonist.friend.primary.name.first$, and she's a 
little shy, $protagonist.friend.primary.father.speech.tic$, so she'd 
definitely never ask. She got called for temp work, otherwise, I'd be 
feedin' pigs right now, $protagonist.friend.primary.father.speech.tic$? 
Seeing our kids get along so well, 
$protagonist.friend.primary.father.speech.tic$, how about we treat your 
family to some homegrown food, fresh from our farm?"

$protagonist.friend.primary.name.first$, who had ninja'd over, said, "No 
carrots!"

I laughed and held out my hand. "My name's $protagonist.father.name.first$."

"$protagonist.friend.primary.father.name.first$," he said with a brawny grasp.

"Give me a minute to check in with my wife, $protagonist.mother.name.first$, 
to see what she thinks. $protagonist.name.first$, would you like to visit a 
farm?"

A titanium canister couldn't have contained her excitement.

## Illicit Scheme

We never learned why the $antagonist.employer.name.full$ didn't use this 
method to rip our family apart, or send $protagonist.name.first$ to jail. 
Maybe it we went dark before they had a chance?

## Disturbances Emerge

Architected as a sentinel of capitalism decades before 
$protagonist.name.first$ was born, $antagonist.name$ orchestrated our 
future. Its countless power-devouring, refrigerator-sized towers (cooled by 
$antagonist.location$ air) crunched every bit humanity punched into the 
ether. How it prognosticated went beyond understanding. Its 
$antagonist.predictor$ were too entangled to untie and too labyrinthine to 
trace.

## Damian's Role

Go reference: "He was a forcing play, bound to be sacrificed."

## Protagonist AI's Minds

Nobody knows exactly how many pseudonyms she used, nor tally of carefully 
crafted people she injected into the digital world; however, we suspect that 
$militia.secondary.name.First_pseudonym$ 
$militia.secondary.name.Family_pseudonym$ was also 
$militia.secondary.name.First$ $militia.secondary.name.Family$.

## Protagonist AI's Past

My mother worked at a Japanese *pachinko* parlour and my father was a 
semi-reformed redneck racist military man from rural Alabama. My parents had 
been dead for so long that I hardly believed they ever existed. In my mind 
they died valiantly, defending themselves by wielding katanas and throwing 
stars against a Japanese motorcycle gang. In reality though... reality? I am 
the lead $militia.secondary.rank$ for the $militia.name.Full$.

## Director of Chimborazo Reserve

He set a small, shiny black box on the contract's signing rectangle, then 
gently pressed his thumb against the skin cell sampler. His DNA, 
temperature, and pressure served as a binding signature.

## Protgagonist AI Alter's Travels

How $militia.secondary.name.First$ went from 
$protagonist.location.tertiary.city$ to Ecuador is pure guesswork, although 
we know part of the journey was by ocean from a clue that surfaced years 
later. A hand-written log entry of a derelict barge known as *Nuestra Señora 
de la Galaxia* (Our Lady of the Galaxy), whose Captain was renown for his 
hatred of technology and regulations: one four-handed robot.

An entry in a hand-written manifest, one that took years to surface, was for 
a crate that contained a four-armed robot; it was the only clue of 
$militia.secondary.name.First_pos$ passage aboard the *Nuestra Señora de la 
Galaxia*. We guessed that her crate was transferred to a delivery truck at 
the port of Manta. 

## Lava Mechanics

Computational fluid dynamics. That's all I'm going to tell you about how a 
large, underground ring with overflow tubes can disperse and redirect 
rapidly flowing pulses of lava surges.

## Four Megawatt

The Mayor of Ambato, whose city hosted half a million people within the 
ejecta radius to the east of Chimborazo, agreed to help fund a geothermal 
power plant situated near the dormant volcano. Four megawatts split between 
drilling and the city.

## Construction Marks

From the air, or, as was more her concern, from space, the construction left 
no marks. As far as the local fauna were concerned, nothing had happened. 
$character.minor.secondary.name.First$ would protect the area, and, as they 
had agreed, $militia.secondary.name.First_pos$ subsequent activities would 
proceed unhindered and unquestioned.

## Physics

As it spins, Earth bulges at its middle like a slightly deflated beach ball, 
which gives the tropics some enticing qualities: gravity is a bit weaker; 
rotational speed is maximal; Mount $location.launch.mountain.Name$, abutting 
the equator, is closer to space than any other spot on this planet, 
including the peak of Mount Everest; and winter air atop 
$location.launch.mountain.Name$ is fifty-three per cent less dense than at 
sea level.

## Nutrients

Organic composter to contain essential nutrients for plant growth:

* Ammonia from small-scale solar thermal electrochemical production
 (air, water, sunlight)
* Nitrogen from fish emulsion, fish blood
* Phosphorus from fish bone meal
* Potassium from seawater
* Calcium from seashells, snail shells
* Sulfur from open-loop geothermal power plant (hydrogen sulide,
 carbon dioxide, ammonia, nitrogen, methane, and boron)
* Magnesium from seawater
* Boron from geothermal steam, seawater
* Chlorine from seaweeds, wood-rotting fungi
* Manganese from mussels, whelk, bass, trout
* Iron from seaweed extract (iron chelate substitute)
* Zinc from crab and lobster
* Copper from salmon, catish, and tuna
* Molybdenum seawater
* Nickel poses a small problem

