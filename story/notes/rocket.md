# Rocket

* initial latitude: 1.469167 degrees
* initial altitude: 6212 m
* diameter: 0.6 m
* initial velocity: Mach 2.9 (916.90 m/s)
* target altitude: 450 km (within Van Allen belt)
* drag: 0.219
* wet mass: 400 kg
* payload mass: 75 kg
* fuel: 285 kg
* specific impulse: 900 s
* rocket count: 6.5 per 500 kg rover-sized vehicle
* 30 rockets (8550 kg fuel)
* 23.948 s (ramp) + 33.631 s (charging) = 57.579 s per launch
* 57.579 s * 30 rockets = 28.79 minutes total launch time
* 289 s (orbit) = 5.8 minutes

1 mini mining operation
1 attack rocket
2 3d printers
3 vehicles
5 solar arrays
10 battery packs

# Moonshot

A mass of 400 kg requires 168 kg of fuel and 14 to 15 hours to reach the moon.

# Assault Timeline

Hoshi needs 116 minutes to finish launching all rockets. Humvee needs to get within 30 km to fire. Humvees dispatched from military base are 103 km away. The Humvees travel at 88 km/hr, driving 104 km in 71 minutes. Flight from nearest air force base takes about 21 minutes; prep takes 25 minutes.

* T-minus 69 minutes - Countdown begins
* T-minus 69 minutes - Informs Salvatierra to evacuate mountain
* T-minus 67 minutes - Salvatierra contacts Military
* T-minus 43 minutes - General Urbina dispatches Humvee, Embraer EMB 314
* T-minus 42 minutes - General Urbina contacts General Mitchell
* T-minus 38 minutes - Humvees en route
* T-minus 31 minutes - General Mitchell dispatches autonomous fighter jets
* T-minus 18 minutes - EMB plane en route
* T-minus 17 minutes - Muon sensors detect possible eruption (two-hour window)
* T-minus 00 minutes - Launch 1st rocket
* T-plus 5.8 minutes - Rocket reaches orbit
* T-plus 6.7 minutes - Rocket releases attack bullets
* T-plus 7 minutes - Launch of 7th rocket
* T-plus 7 minutes - Arrival of EMB
* T-plus 28 minutes - Arrival of Humvee
* T-plus 116 minutes - Launch of 30th rocket

# Railgun

e = (1/2) * m * v^2

Mass (m) = 400 kg
Velocity = 916.9 m/s
Energy (e) = 168.14 MJ = 46.71 kWh

Power Plant = 5 MW

Time = 46.71 kWh / 5 MW = 33.631 seconds

# Acceleration

a = (v^2 - u^2) / 2 * d

Target velocity (v) = 916.9 m/s
Initial velocity (u) = 0 m/s
Distance (d) = 10978 m
Acceleration (a) = 38.29 m/s^2

Time = 23.948 seconds

