# Overview

Drawings---to be included in a science fiction novel---span various related concepts:

* Food and money
* Wealth distribution
* Drake equation
* Food production and distribution model
* Capitalistic advertising to bootstrap development
* Idealized town layout for infrastructure

The illustrations are meant to be drawn by a single person whose artistry improves over time. Parenthetical numbers in the section titles that follow are the artist's age, in years. In this document, the images are rough drafts meant to convey concepts.

## Food (8)

Represents the recursive relationship that food has to tangible goods (ignoring services and rather simplified). For example, with each $10 shirt sold, a shirt's vendor spends: $3 on food, $4 on rent, and $3 on goods. For each $4 in rent, the landlord spends: $1.20 on food, $1.20 on property payments, and $1.6 on goods. The chain continues until most money has been spent on food.

![wealth](images/diminish.svg)

## Wealth (12)

Correlates wealth with power by comparing social structures across time and place. From the top, the hierarchy resembles a beehive; from the bottom, it looks like a dung heap. Farming is emphasized to show how food has always been a major source of wealth (bottom-up economics).

![societies](images/social-structure.svg)

## Equation (14)

The Drake Equation helps us reason about the number of intelligent civilizations that could exist in the galaxy. The equation is often expressed in terms of the following factors:

* Number of stars that form per year ($R_{*}$)
* Percentage of stars that have planets
* Percentage of planets in star's habitable zone (i.e., liquid water)
* Percentage of habitable planets where life evolves
* Percentage of evolutionary paths leading to technology-enabled civilizations
* Percentage of civilizations that send detectable communications signals
* Length of time that each civilization sends such signals

Using mathematical notation:

$N = R_{*} \cdot f_{p} \cdot n_{e} \cdot f_{l} \cdot f_{i} \cdot f_{c} \cdot L$

Using pictures:

![societies](images/drake-equation.svg)

Explores the idea of resource acquisition by extraterrestrial intelligences, which leads to realization that artificial general intelligence applied to food production is reasonably safe.

## Model (16)

A high-level concept depicting a self-sustaining system that removes humans from all facets of food manufacturing. Without humans involved in food production, nobody must be remunerated for their time, significantly decreasing food costs. By eliminating capitalism's main source of wealth, a new societal power structure may rise---possibly socialism-driven egalitarianism with strong ties to humanitarianism and environmental conservation.

![system](images/model.svg)

## Bootstrap (19)

Depicts a futuristic "e-Poster" that's displayed across an electronic advertising network in the real-world (e.g., ads on street lamps, in store windows, sides of city buses, etc.). The ad calls for assembly of specialists across a variety of trades. The tagline, *Work for a Workless Future*, embodies the project's goal. Also shown is a symbol of how a "workless future" may be achieved: through robotic automation.

![poster](images/poster.svg)

## Infrastructure (23)

Proposes a physical layout for a modern community based on the model.

![infrastructure](images/landscape.svg)

# Technology

Drawing these illustrations will lean heavily on the following tools and skills, which can be learned along the way, if needed:

* Inkscape software
* Nodes, alignment, gradients, icon libraries
* Bezier curves
* Rasterization vs. vectorization
* Editing XML documents

A tablet, such as the [Wacom PTH660](https://www.amazon.com/dp/B01MQU5LW7) or [XP-Pen Deco 03](https://www.amazon.ca/dp/B07BF9GNFD), may be useful. If you don't have one, I'll order one for you.
