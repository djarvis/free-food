* Can she carry her own story?
  - wants to supercharge phytoplankton to help sequester CO2
    using natural and artificial whale pumps
* Is she three-dimensional?
  - lawful good; acts with honor and a sense of duty
  - conflicted between helping Yuna and reporting
    + Yuna might be able to help with the whales
* Does she do more than just represent an idea?
  - fights against a capitalistic system profiting from whaling
* Does she have flaws?
  - puts Chloe ahead of her own needs

