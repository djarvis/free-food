#!/usr/bin/env bash

readonly HOST_FILE=host.txt
readonly TYPESETTER_HOME="$HOME/dev/keenwrite"
readonly TYPESETTER_FILE="keenwrite.bin"
readonly TEMP_NAME=/tmp/autonoma.pdf

if [ "$#" -ne 3 ]; then
  echo "Usage: $BASH_SOURCE first_name other_names email"
  exit
fi

if [ ! -f "${HOST_FILE}" ]; then
  echo "Create ${HOST_FILE}"
  exit
fi

if ! command -v "${TYPESETTER_FILE}"; then
  echo "Add KeenWrite binary to PATH"
  exit
fi

${TYPESETTER_FILE} \
    --all \
    --r-dir="$PWD/bin" \
    --r-script="$PWD/bin/editor.R" \
    --image-dir="$PWD/images" \
    --variables="$PWD/variables.yaml" \
    --theme-dir="${TYPESETTER_HOME}/themes/boschet" \
    --metadata=title={{document.title}} \
    --metadata=byline={{document.author.byline}} \
    --metadata=keywords={{document.keywords}} \
    --metadata=copyright={{document.copyright}} \
    --metadata="reviewer=$1 $2, $3" \
    --set="document.reviewer=$1 $2" \
    --chapters="-17,20" \
    --input="$PWD/chapter/01.Rmd" \
    --output="${TEMP_NAME}"

readonly NAME=$(echo $1 $2 | tr '[:upper:]' '[:lower:]' | tr ' ' '-')
readonly FILENAME="autonoma-${NAME}.pdf"
readonly HOST=$(cat "${HOST_FILE}")

scp -q "${TEMP_NAME}" "${HOST}:www/alpha/${FILENAME}"

readonly SUBJ=$(cat title.txt)
readonly LINK="https://autonoma.ca/alpha/${FILENAME}"

thunderbird -compose "subject='$SUBJ',to='$1 $2 <$3>',body=$LINK"

