#!/usr/bin/env bash

# Builds the manuscript based on the reference document style.

# Filename to update
MANUSCRIPT=$(cat title.txt).docx

REFERENCE_DIR=docx

# Silent chmod (fail quietly).
chmod -f 644 $MANUSCRIPT

# Source directory.
ARG_MARKDOWN=/tmp/overview

mkdir -p $ARG_MARKDOWN

# Use the reference document (template) to create the DOCX version.
pandoc \
  --reference-doc=$REFERENCE_DIR/reference.docx \
  -o $MANUSCRIPT \
  -s $ARG_MARKDOWN/*.md

WORKING_DIR=/tmp/replacement/
DOC_DIR=$WORKING_DIR/word

rm -rf $WORKING_DIR
mkdir -p $WORKING_DIR

# Modify the DOCX file directly.
unzip $MANUSCRIPT -d $WORKING_DIR

# Replace the horizontal rule with a centered hashmark.
sed -e 's!<w:p><w:r><w:pict><v:rect style="width:0;height:1.5pt" o:hralign="center" o:hrstd="t" o:hr="t" /></w:pict></w:r></w:p>!<w:p><w:pPr><w:pStyle w:val="Normal"/><w:jc w:val="center"/><w:rPr></w:rPr></w:pPr><w:r><w:rPr></w:rPr><w:t>\#</w:t></w:r></w:p>!g' $DOC_DIR/document.xml > $DOC_DIR/document.xml.tmp && mv $DOC_DIR/document.xml.tmp $DOC_DIR/document.xml

# Keep the chapter heading style (to include the chapter number)
cp $REFERENCE_DIR/*xml $DOC_DIR

# Repack the document.
pushd $WORKING_DIR
zip -9 -r /tmp/$MANUSCRIPT .
popd
rm -rf $WORKING_DIR

# Replace the existing manuscript with the revised version.
mv /tmp/$MANUSCRIPT .

