/**
 * The purpose of this code is to help calculate parameters required to
 * send a rocket into space using a combination of single-stage to orbit
 * (SSO) and an electromagnetic accelerator.
 */

// Per 1 m/s^2
var GRAVITATIONAL_ACCELERATION = 0.10193679918451;

// Shift magnitude
var MAGNITUDE = 1000;

// Convert from degrees C to K
var CELSIUS_KELVIN = 273.15;

// Specific gas constant for dry air
var GAS_CONSTANT = 287.05;

$(document).ready( function() {
  var fn_val = $.fn.val;

  /**
   * Avoid duplicate calls to parseFloat by overriding jQuery's val().
   */
  $.fn.val = function( value ) {
    // val() can be called with or without arguments: retain the behaviour.
    var result = (arguments.length >= 1) ?
      fn_val.call( this, value ) : fn_val.call( this );

    return parseFloat( result );
  };

  /**
   * Recalculate the totals when an input value changes.
   */
  $(".variable").change( function() {
    var accelerator_radius = $("#accelerator_radius").val();
    var projectile_velocity = $("#projectile_velocity").val();
    var rocket_mass = $("#rocket_mass").val();
    var rocket_payload = $("#rocket_payload").val();
    var rocket_drag = $("#rocket_drag").val();
    var rocket_cross_section = $("#rocket_cross_section").val();
    var fuel_mass = $("#fuel_mass").val();

    // v^2
    var velocity_2 = projectile_velocity * projectile_velocity;
    // v^3
    var velocity_3 = velocity_2 * projectile_velocity;

    // Calculate maximum force exerted at peak acceleration.
    var total_centrifugal_force = velocity_2 / accelerator_radius;
    $("#total_centrifugal_force").val( total_centrifugal_force );

    // Convert maximum force exerted at peak acceleration into G forces.
    var total_centrifugal_force_g = 
      total_centrifugal_force * GRAVITATIONAL_ACCELERATION
    $("#total_centrifugal_force_g").val( total_centrifugal_force_g );

    var atmosphere_altitude = $("#atmosphere_altitude").val();
    var atmosphere_temperature = $("#atmosphere_temperature").val();
    var atmosphere_sea_level = $("#atmosphere_sea_level").val();

    var h = 0.0065 * atmosphere_altitude;
    var t = CELSIUS_KELVIN + atmosphere_temperature;

    // Calculate air pressure.
    var total_air_pressure = atmosphere_sea_level * Math.pow(
      1 - (h / (h + t)), 5.257
    );
    $("#total_air_pressure").val( total_air_pressure );

    // Calculate and convert air density into kilograms.
    var total_air_density =
      total_air_pressure / (GAS_CONSTANT * t);
    $("#total_air_density").val( total_air_density );

    // Calculate drag force.
    var total_drag_force = 0.5
      * total_air_density * velocity_2
      * rocket_drag * rocket_cross_section;
    $("#total_drag_force").val( total_drag_force );

    // Compute total rocket mass.
    var total_rocket_mass = rocket_mass + rocket_payload + fuel_mass;
    $("#total_mass").val( total_rocket_mass );

    // Compute total rocket dry mass.
    var total_rocket_dry_mass = total_rocket_mass - fuel_mass;
    $("#total_dry_mass").val( total_rocket_dry_mass );

    // Calculate drag deceleration.
    var total_deceleration = total_drag_force / total_rocket_mass;
    $("#total_deceleration").val( total_deceleration );

    // Convert drag deceleration into G forces.
    var total_deceleration_g = total_deceleration * GRAVITATIONAL_ACCELERATION;
    $("#total_deceleration_g").val( total_deceleration_g );

    // 0.1 * 0.5 * (0.196 m^2) * (0.627 kg/m^3) * ((8000 m/s)^3)

    // Calculate friction in megawatts.
    var total_friction = 0.1 * rocket_drag * rocket_cross_section *
      total_air_density * velocity_3 / MAGNITUDE / MAGNITUDE;
    $("#total_friction").val( total_friction );

    // Display totals as comma-separated numbers.
    // See: https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString
    $("#totals").find("input").each( function() {
      if( $(this).val() ) {
        $(this).val( $(this).val().toLocaleString() );
      }
    });
  });

  // Force totals calculation on page load.
  $("#rocket_height").trigger( "change" );
});
