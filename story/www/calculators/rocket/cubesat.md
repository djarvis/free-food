
CubeSat
----
Dimensions = 100 mm^3
Mass = 1.33 kg

Sentient Machine Brain
----
Dimensions = 300 mm^3
Material = Titanium Dioxide
Shape = Circle
Pieces = 7
Diamter = 30 cm
Thickness = 1 mm
Mass = 2.23133448 kg
Calculator = https://www.twmetals.com/calculators.html

Sentient Machine Brain CubeSat
----
Dimensions = 300 mm^3
Payload = 4 kg + 2.2313348 kg = 6.2313348 kg ~= 6.25 kg

Sentient Machine Rocket
----
Height = 2 m
Diameter = 0.5 m
Mass = 700 kg
Chamber Material = Halfnium, nitrogen, and graphene
Chamber Temperature = 5600 K
Fuel Material = Metallic hydrogen
Fuel Weight = 300 kg
Specific Impulse = 1700 s

Delta-V for LEO = 9,300 m/s
Delta-V for Lunar = 16,400 km/s

