<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

  <xsl:import href="../template.xsl" />

	<xsl:template name="custom-stylesheet">
		<link
			rel="stylesheet"
			href="//cdnjs.cloudflare.com/ajax/libs/KaTeX/0.8.3/katex.min.css" />
	</xsl:template>

  <xsl:template match="/root" mode="header">
    <h1>Aerodynamic Heat Calculator</h1>
  </xsl:template>

  <xsl:template match="/root" mode="content">
    <p>
    Helps determine skin surface temperature of a conical-nosed projectile
    at various altitudes and velocities. Form field values are explained
    in the <a href="#equations">Equations</a> section, below.
    </p>

    <form id="calculator">
    <fieldset id="fieldset_atmosphere">
      <legend>Atmosphere</legend>

      <label for="atmosphere_sea_level">Sea Level Pressure (Pa)</label>
      <input tabindex="1" autofocus="autofocus"
        class="variable" type="number" step="any" min="0" value="101325"
        id="atmosphere_sea_level" name="atmosphere_sea_level" />

      <label for="atmosphere_temperature">Temperature (&#x2103;)</label>
      <input tabindex="2"
        class="variable" type="number" step="0.1" min="-80" value="-7"
        id="atmosphere_temperature" name="atmosphere_temperature" />

      <label for="atmosphere_altitude">Altitude (m)</label>
      <input tabindex="3"
        class="variable" type="number" step="0.1" min="0" value="6268"
        id="atmosphere_altitude" name="atmosphere_altitude" />
    </fieldset>

    <fieldset id="fieldset_accelerator">
      <legend>Electromagnetic Accelerator Loop</legend>

      <label for="accelerator_radius">Radius (m)</label>
      <input tabindex="4"
        class="variable" type="number" step="0.1" min="0.1" value="5000"
        id="accelerator_radius" name="accelerator_radius" />

      <label for="projectile_velocity">Projectile Exit Velocity (m/s)</label>
      <input tabindex="5"
        class="variable" type="number" step="0.1" min="0" value="1000"
        id="projectile_velocity" name="projectile_velocity" />
    </fieldset>

    <fieldset id="fieldset_rocket">
      <legend>Rocket</legend>

      <label for="rocket_height">Height (m)</label>
      <input tabindex="6"
        class="variable" type="number" step="0.1" min="0" value="2"
        id="rocket_height" name="rocket_height"  />

      <label for="rocket_diameter">Diameter (m)</label>
      <input tabindex="7"
        class="variable" type="number" step="0.1" min="0" value="0.5"
        id="rocket_diameter" name="rocket_diameter"  />

      <label for="rocket_mass">Body Mass (kg)</label>
      <input tabindex="8"
        class="variable" type="number" step="0.1" min="0" value="700"
        id="rocket_mass" name="rocket_mass" />

      <label for="rocket_payload">Payload (kg)</label>
      <input tabindex="9"
        class="variable" type="number" step="0.01" min="0" value="6.25"
        id="rocket_payload" name="rocket_payload" />

      <label for="rocket_cross_section">Cross-section (m<sup>2</sup>)</label>
      <input tabindex="10"
        class="variable" type="number" step="any" min="0" value="0.196"
        id="rocket_cross_section" name="rocket_cross_section" />

      <label for="rocket_drag">Coefficient of Drag</label>
      <input tabindex="11"
        class="variable" type="number" step="any" min="0" value="0.4"
        id="rocket_drag" name="rocket_drag" />
    </fieldset>

    <fieldset id="fieldset_fuel">
      <legend>Fuel</legend>

      <label for="fuel_mass">Mass (kg)</label>
      <input tabindex="12"
        class="variable" type="number" step="0.1" min="0" value="300"
        id="fuel_mass" name="fuel_mass"  />

      <label for="fuel_specific_impulse">Specific Impulse (s)</label>
      <input tabindex="13"
        class="variable" type="number" step="0.1" min="0" value="1700"
        id="fuel_specific_impulse" name="fuel_specific_impulse"  />
    </fieldset>

    <fieldset id="totals">
      <legend
        class="totals">Total</legend>

      <label for="total_mass">Launch Mass (kg)</label>
      <input
        class="variable" type="text" value="" readonly="readonly"
        id="total_mass" name="total_mass"  />

      <label for="total_dry_mass">Dry Mass (kg)</label>
      <input
        class="variable" type="text" value="" readonly="readonly"
        id="total_dry_mass" name="total_dry_mass"  />

      <label for="total_air_pressure">Air Pressure (Pa)</label>
      <input
        class="variable" type="text" value="" readonly="readonly"
        id="total_air_pressure" name="total_air_pressure" />

      <label for="total_air_density">Air Density (kg/m<sup>3</sup>)</label>
      <input
        class="variable" type="text" value="" readonly="readonly"
        id="total_air_density" name="total_air_density" />

      <label for="total_drag_force">Drag Force (N)</label>
      <input
        class="variable" type="text" value="" readonly="readonly"
        id="total_drag_force" name="total_drag_force" />

      <label for="total_decleration">Drag Deceleration (m/s<sup>2</sup>)</label>
      <input
        class="variable" type="text" value="" readonly="readonly"
        id="total_deceleration" name="total_deceleration" />

      <label for="total_decleration_g">Drag Deceleration (g-force)</label>
      <input
        class="variable" type="text" value="" readonly="readonly"
        id="total_deceleration_g" name="total_deceleration_g" />

      <label for="total_centrifugal_force">Centrifugal Force (km/s<sup>2</sup>)</label>
      <input
        class="variable" type="text" value="" readonly="readonly"
        id="total_centrifugal_force" name="total_centrifugal_force" />

      <label for="total_centrifugal_force_g">Centrifugal Force (g-force)</label>
      <input
        class="variable" type="text" value="" readonly="readonly"
        id="total_centrifugal_force_g" name="total_centrifugal_force_g" />

      <label for="total_friction">Friction (MW)</label>
      <input
        class="variable" type="text" value="" readonly="readonly"
        id="total_friction" name="total_friction" />
    </fieldset>
    </form>

    <a name="equations" />
    <h1>Equations</h1>
    <p>
      This section describes equation inputs and outputs.
    </p>

    <h2>Centrifugal force</h2>
    <p class="equation">
    $$a = v^2 / r$$
    $$g_{force} = 0.10193679918451 a$$
    </p>
    <div class="variables">
      <div class="input">
        <h3>Inputs</h3>
        <dl>
          <dt>$v$</dt>
          <dd>Projectile exit velocity (km/s)</dd>
          <dt>$r$</dt>
          <dd>Accelerator loop radius (km)</dd>
        </dl>
      </div>
      <div class="output">
        <h3>Outputs</h3>
        <dl>
          <dt>$a$</dt>
          <dd>Acceleration (km/s<sup>2</sup>)</dd>
          <dt>$g_{force}$</dt>
          <dd>Force experienced in terms of Earth's gravitational acceleration</dd>
        </dl>
      </div>
    </div>

    <h2>Cross-section area</h2>
    <p class="equation">
    $$A = \pi (d / 2)^2$$
    </p>
    <div class="variables">
      <div class="input">
        <h3>Input</h3>
        <dl>
          <dt>$d$</dt>
          <dd>Rocket diameter (m)</dd>
        </dl>
      </div>
      <div class="output">
        <h3>Output</h3>
        <dl>
          <dt>$A$</dt>
          <dd>Rocket's cross-section area (m<sup>2</sup>)</dd>
        </dl>
      </div>
    </div>
    <p>
    A complete cross-section requires a far more complex calculation. The
    value from that calculation can be provided as an input to the calculator.
    </p>

    <h2>Air pressure</h2>
    <p class="equation">
    $$P = P_{sea} \left(1 - \frac{0.0065 h}{0.0065 h + T + 273.15}\right)^{5.257}$$
    </p>
    <div class="variables">
      <div class="input">
        <h3>Input</h3>
        <dl>
          <dt>$h$</dt>
          <dd>Altitude (m)</dd>
          <dt>$T$</dt>
          <dd>Temperature (&#x00b0;C)</dd>
          <dt>$P_{sea}$</dt>
          <dd>Sea level pressure (1013.25 hPa)</dd>
        </dl>
      </div>
      <div class="output">
        <h3>Output</h3>
        <dl>
          <dt>$P$</dt>
          <dd>Air pressure (Pa)</dd>
        </dl>
      </div>
    </div>

    <h2>Air density</h2>
    <p class="equation">
    $$\rho = P / R (T + 273.15)$$
    </p>
    <div class="variables">
      <div class="input">
        <h3>Inputs</h3>
        <dl>
          <dt>$P$</dt>
          <dd>Air pressure (Pa)</dd>
          <dt>$R$</dt>
          <dd>Specific gas constant for dry air (287.05&#160;J/(kg⋅K))</dd>
          <dt>$T$</dt>
          <dd>Temperature (&#x00b0;C)</dd>
        </dl>
      </div>
      <div class="output">
        <h3>Output</h3>
        <dl>
          <dt>$\rho$</dt>
          <dd>Air density (kg/m<sup>3</sup>)</dd>
        </dl>
      </div>
    </div>

    <h2>Drag force</h2>
    <p class="equation">
    $$F_d = \frac{1}{2} \rho v^2 C_d A$$
    </p>
    <div class="variables">
      <div class="input">
        <h3>Inputs</h3>
        <dl>
          <dt>$\rho$</dt>
          <dd>Air density (kg/m<sup>3</sup>)</dd>
          <dt>$v$</dt>
          <dd>Projectile exit velocity (km/s)</dd>
          <dt>$C_d$</dt>
          <dd>Coefficient of drag</dd>
          <dt>$A$</dt>
          <dd>Cross-section area (m<sup>2</sup>)</dd>
        </dl>
      </div>
      <div class="output">
        <h3>Output</h3>
        <dl>
          <dt>$F_d$</dt>
          <dd>Drag force (kN)</dd>
        </dl>
      </div>
    </div>

    <h2>Drag deceleration</h2>
    <p class="equation">
    $$a_d = F_d / M$$
    </p>
    <div class="variables">
      <div class="input">
        <h3>Inputs</h3>
        <dl>
          <dt>$F_d$</dt>
          <dd>Drag force (kN)</dd>
          <dt>$M$</dt>
          <dd>Mass (kg)</dd>
        </dl>
      </div>
      <div class="output">
        <h3>Output</h3>
        <dl>
          <dt>$a_d$</dt>
          <dd>Drag deceleration (m/s<sup>2</sup>)</dd>
        </dl>
      </div>
    </div>

    <h2>Friction</h2>
    <p class="equation">
    $$F = 0.1 C_d A a_d v^3$$
    </p>
    <div class="variables">
      <div class="input">
        <h3>Inputs</h3>
        <dl>
          <dt>$C_d$</dt>
          <dd>Coefficient of drag</dd>
          <dt>$A$</dt>
          <dd>Cross-section area (m<sup>2</sup>)</dd>
          <dt>$a_d$</dt>
          <dd>Drag deceleration (m/s<sup>2</sup>)</dd>
          <dt>$v$</dt>
          <dd>Projectile exit velocity (km/s)</dd>
        </dl>
      </div>
      <div class="output">
        <h3>Output</h3>
        <dl>
          <dt>$F$</dt>
          <dd>Heat experienced by rocket due to friction</dd>
        </dl>
      </div>
    </div>
  </xsl:template>

	<xsl:template name="custom-scripts">
		<script
			src="//cdnjs.cloudflare.com/ajax/libs/KaTeX/0.8.3/katex.min.js" />
		<script
			src="//cdnjs.cloudflare.com/ajax/libs/KaTeX/0.8.3/contrib/auto-render.min.js" />
		<script>
      renderMathInElement(
				document.body, {
					delimiters: [
						{ left:  "$$", right:  "$$", display:  true },
						{ left: "\\[", right: "\\]", display:  true },
						{ left:   "$", right:   "$", display: false },
						{ left: "\\(", right: "\\)", display: false }
					]
				}
      );
		</script>
	</xsl:template>

</xsl:stylesheet>

