<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

  <xsl:import href="../index.xsl" />

  <xsl:template match="/root" mode="styles">
    <xsl:variable name="relative" select="'../../'" />
    <xsl:call-template name="stylesheet">
      <xsl:with-param name="file" select="'simple'" />
      <xsl:with-param name="relative" select="$relative" />
    </xsl:call-template>
    <xsl:call-template name="stylesheet">
      <xsl:with-param name="file" select="'form'" />
      <xsl:with-param name="relative" select="$relative" />
    </xsl:call-template>
    <xsl:call-template name="stylesheet">
      <xsl:with-param name="file" select="'calculator'" />
      <xsl:with-param name="relative" select="$relative" />
    </xsl:call-template>

    <xsl:call-template name="custom-stylesheet" />
  </xsl:template>

  <xsl:template name="custom-stylesheet" />

  <xsl:template match="/root" mode="scripts">
    <script
      defer="defer"
      src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" />
    <xsl:call-template name="script">
      <xsl:with-param name="file" select="'calculator'" />
    </xsl:call-template>

    <xsl:call-template name="custom-scripts" />
  </xsl:template>

  <xsl:template name="custom-scripts" />

</xsl:stylesheet>
