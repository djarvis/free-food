/**
 * The purpose of this code is to help calculate the energy production of
 * a vertical farm. The energy production is contrasted with the energy
 * consumption of a local population.
 */

/**
 * Global for non-leap year.
 */
var DAYS_PER_YEAR = 365;

/**
 * Global for cent to dollar conversion.
 */
var CENTS_PER_DOLLAR = 100;

$(document).ready( function() {
	if( $.valHooks.text ) {
    originalHook = $.valHooks.text.get;
	}
  else {
    $.valHooks.text = {};
  }

  $.valHooks.text.get = function( el ) {
		var result = parseFloat( el.value );
		return isNaN( result ) ? el.value : result;
  }

  /**
   * Recalculate the totals when an input value changes.
   */
  $(".variable").change( function() {
    var building_tally   = $("#building_tally").val();
    var building_length  = $("#building_length").val();
    var building_width   = $("#building_width").val();
    var building_storeys = $("#building_storeys").val();

    var building_infrastructure = $("#building_infrastructure").val();

    // Crop area per storey.
    var total_building_storey_area = Math.floor(
      (building_length * building_width) * (1 - building_infrastructure) );

    // Racks per storey.
    var building_storey_height = $("#building_storey_height").val();
    var crop_height = $("#crop_height").val();
    var total_racks_per_storey =
      Math.floor( building_storey_height / crop_height );
    $("#total_racks_per_storey").val( total_racks_per_storey );

    // Harvestable area per building.
    var total_harvestable_area =
      total_building_storey_area * building_storeys * total_racks_per_storey;

    // Area per crop.
    var crop_diameter = $("#crop_diameter").val();
    var crop_area = Math.pow( (crop_diameter / 2), 2 ) * Math.PI;

    // Plants per building.
    var total_plants_per_building =
      Math.floor( total_harvestable_area / crop_area );
    $("#total_plants_per_building").val( total_plants_per_building );

    // Energy per plant.
    var crop_unit_energy = $("#crop_unit_energy").val();
    var crop_units_per_plant = $("#crop_units_per_plant").val();

    var total_energy_per_plant = crop_unit_energy * crop_units_per_plant;
    $("#total_energy_per_plant").val( total_energy_per_plant );

    // Energy per harvest.
    var total_energy_per_harvest =
      total_energy_per_plant * total_plants_per_building;
    $("#total_energy_per_harvest").val( total_energy_per_harvest );

    // Annual energy production.
    var crop_harvests = $("#crop_harvests").val();
    var total_energy_production =
      total_energy_per_harvest * crop_harvests * building_tally;
    $("#total_energy_production").val( total_energy_production );

    // Annual energy consumption.
    var people_population = $("#people_population").val();
    var people_energy = $("#people_energy").val();

    var total_energy_consumption =
      people_population * people_energy * DAYS_PER_YEAR;
    $("#total_energy_consumption").val( total_energy_consumption );

    var lamp_coverage_fixture = $("#lamp_coverage_fixture").val();
    var conveyer_ratio = $("#conveyer_ratio").val();

    $("#total_area").val( total_harvestable_area );

    // Number of lamps in fixed positions (without conveyance).
    var lamps_fixed = total_harvestable_area / lamp_coverage_fixture;

    // Number of lamps accounting for conveyance.
    var lamps_moving = lamps_fixed - (conveyer_ratio * lamps_fixed);

    $("#total_lamps").val( Math.ceil( lamps_moving ) );

    // Number of conveyers required for the lamps.
    var conveyers = lamps_fixed - lamps_moving;

    $("#total_conveyers").val( Math.ceil( conveyers ) );

    // Power consumption per lamp.
    var lamp_power = $("#lamp_power").val();

    // Total power required by all lamps.
    var lamps_power = Math.ceil( lamps_moving * lamp_power );

    $("#lamps_power").val( lamps_power );

    // Power consumption per conveyer.
    var conveyer_power = $("#conveyer_power").val();

    // Total power required by all conveyers.
    var conveyers_power = Math.ceil( conveyers * conveyer_power );

    $("#conveyers_power").val( conveyers_power );

    // Total power devoted to illumination.
    var total_electric_consumption = Math.ceil( lamps_power + conveyers_power );

    // Amount of wattage consumed.
    $("#total_electric_consumption").val( total_electric_consumption );

    var cost_kWh = $("#cost_kWh").val();
    var cost_daily_usage = $("#cost_daily_usage").val();

    var cost_per_hour = (total_electric_consumption / 1000) * cost_kWh;
    var cost_per_day = cost_daily_usage * cost_per_hour;
    var cost_per_year = cost_per_day * DAYS_PER_YEAR;

    var total_electric_cost = Math.ceil( cost_per_year );

    $("#total_electric_cost").val( total_electric_cost / CENTS_PER_DOLLAR );

    // Display totals as comma-separated numbers.
    // See: https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString
    $("#totals").find("input").each( function() {
      if( $(this).val() ) {
        $(this).val( $(this).val().toLocaleString() );
      }
    });
  });

  // Force totals calculation on page load.
  $("#people_population").trigger( "change" );
});
