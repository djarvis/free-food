<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

  <xsl:import href="../template.xsl" />

  <xsl:template match="/root" mode="header">
    <h1>Vertical Farm Energy Calculator</h1>
  </xsl:template>

  <xsl:template match="/root" mode="content">
    <p>
    Food production calculator for renewable-powered vertical farms to
    estimate local population sustenance requirements. Neither
    nutritional requirements nor crop variety are considered.
    </p>

    <form id="calculator">
      <fieldset id="fieldset_people">
        <legend>People</legend>

        <label for="people_population">
          Population
        </label>
        <input tabindex="1" autofocus="autofocus"
          id="people_population"
          name="people_population"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/population/actual" />
          </xsl:attribute>
        </input>

        <label for="people_energy">
          Daily Energy Requirements
          (<xsl:apply-templates select="farm/energy/unit" />)
        </label>
        <input tabindex="2"
          id="people_energy"
          name="people_energy"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/energy/value" />
          </xsl:attribute>
        </input>
      </fieldset>

      <fieldset id="fieldset_building">
        <legend>Building</legend>

        <label for="building_tally">
          Tally
        </label>
        <input tabindex="3"
          id="building_tally"
          name="building_tally"
          class="variable" type="number" step="any" min="0" value="1" />

        <label for="building_length">
          Length
          (<xsl:apply-templates select="farm/building/length/unit" />)
        </label>
        <input tabindex="4"
          id="building_length"
          name="building_length"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/building/length/value" />
          </xsl:attribute>
        </input>

        <label for="building_width">
          Width
          (<xsl:apply-templates select="farm/building/width/unit" />)
        </label>
        <input tabindex="5"
          id="building_width"
          name="building_width"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/building/width/value" />
          </xsl:attribute>
        </input>

        <label for="building_storey_height">
          Storey Height
          (<xsl:apply-templates select="farm/building/storeys/height/unit" />)
        </label>
        <input tabindex="6"
          id="building_storey_height"
          name="building_storey_height"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/building/storeys/height/value" />
          </xsl:attribute>
        </input>

        <label for="building_storeys">
          Storeys
        </label>
        <input tabindex="7"
          id="building_storeys"
          name="building_storeys"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/building/storeys/total" />
          </xsl:attribute>
        </input>

        <label for="building_infrastructure">
          Infrastructure (ratio)
        </label>
        <input tabindex="8"
          id="building_infrastructure"
          name="building_infrastructure"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/building/interstitial" />
          </xsl:attribute>
        </input>
      </fieldset>

      <fieldset id="fieldset_crop">
        <legend>
					Crop (e.g., <xsl:apply-templates select="farm/crop/name" />)
				</legend>

        <label for="crop_height">
          Height
          (<xsl:apply-templates select="farm/crop/height/unit" />)
				</label>
        <input tabindex="9"
          id="crop_height"
          name="crop_height"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/crop/height/value" />
          </xsl:attribute>
				</input>

        <label for="crop_diameter">
          Diameter
          (<xsl:apply-templates select="farm/crop/diameter/unit" />)
        </label>
        <input tabindex="10"
          id="crop_diameter"
          name="crop_diameter"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/crop/diameter/value" />
          </xsl:attribute>
				</input>

        <label for="crop_unit_energy">
          Unit Energy
          (<xsl:apply-templates select="farm/crop/energy/unit" />)
        </label>
        <input tabindex="11"
          id="crop_unit_energy"
          name="crop_unit_energy"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/crop/energy/value" />
          </xsl:attribute>
				</input>

        <label for="crop_units_per_plant">
          Units per Plant
        </label>
        <input tabindex="12"
          id="crop_units_per_plant"
          name="crop_units_per_plant"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/crop/yield" />
          </xsl:attribute>
        </input>

        <label for="crop_harvests">
          Annual Harvests
        </label>
        <input tabindex="13"
          id="crop_harvests"
          name="crop_harvests"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/crop/harvests" />
          </xsl:attribute>
				</input>
      </fieldset>

      <fieldset id="fieldset_lamp">
        <legend>
					Grow Lamp
				</legend>

        <label for="lamp_power">
          Power
          (<xsl:apply-templates select="farm/lamp/power/unit" />)
        </label>
        <input tabindex="14"
          id="lamp_power"
          name="lamp_power"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/lamp/power/value" />
          </xsl:attribute>
				</input>

        <label for="lamp_efficacy">
          Luminous Efficacy
          (<xsl:apply-templates select="farm/lamp/efficacy/unit" />)
        </label>
        <input tabindex="15"
          id="lamp_efficacy"
          name="lamp_efficacy"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/lamp/efficacy/value" />
          </xsl:attribute>
				</input>

        <label for="lamp_length">
          Length
          (<xsl:apply-templates select="farm/lamp/length/unit" />)
        </label>
        <input tabindex="16"
          id="lamp_length"
          name="lamp_length"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/lamp/length/value" />
          </xsl:attribute>
				</input>

        <label for="lamp_width">
          Width
          (<xsl:apply-templates select="farm/lamp/width/unit" />)
        </label>
        <input tabindex="17"
          id="lamp_width"
          name="lamp_width"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/lamp/width/value" />
          </xsl:attribute>
				</input>

        <label for="lamp_height">
          Height
          (<xsl:apply-templates select="farm/lamp/height/unit" />)
        </label>
        <input tabindex="18"
          id="lamp_height"
          name="lamp_height"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/lamp/height/value" />
          </xsl:attribute>
				</input>

        <label
          for="lamp_coverage_fixture"
          title="Total area illuminated by a lamp">
          Coverage
          (<xsl:apply-templates select="farm/lamp/coverage/fixture/unit" />)
        </label>
        <input tabindex="19"
          id="lamp_coverage_fixture"
          name="lamp_coverage_fixture"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/lamp/coverage/fixture/value" />
          </xsl:attribute>
				</input>

        <label
          for="conveyer_ratio"
          title="Total area extended by a lamp mover">
          Conveyance Ratio
        </label>
        <input tabindex="20"
          id="conveyer_ratio"
          name="conveyer_ratio"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/lamp/coverage/conveyance/ratio" />
          </xsl:attribute>
				</input>

        <label
          for="conveyer_power"
          title="Electricity required for a lamp mover">
          Conveyance Power
          (<xsl:apply-templates select="farm/lamp/coverage/conveyance/power/unit" />)
        </label>
        <input tabindex="21"
          id="conveyer_power"
          name="conveyer_power"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/lamp/coverage/conveyance/power/value" />
          </xsl:attribute>
				</input>
      </fieldset>

      <fieldset id="fieldset_costs">
        <legend>Electricity</legend>

        <label for="cost_kWh">
          Price
          (<xsl:apply-templates select="farm/cost/electricity/unit" />)
        </label>
        <input tabindex="22"
          id="cost_kWh"
          name="cost_kWh"
          class="variable" type="number" step="any" min="0">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/cost/electricity/value" />
          </xsl:attribute>
				</input>

        <label for="cost_daily_usage">
          Daily Usage
          (<xsl:apply-templates select="farm/cost/electricity/usage/unit" />)
        </label>
        <input tabindex="23"
          id="cost_daily_usage"
          name="cost_daily_usage"
          class="variable" type="number" step="any" min="1">

          <xsl:attribute name="value">
            <xsl:value-of select="farm/cost/electricity/usage/value" />
          </xsl:attribute>
				</input>
      </fieldset>

      <fieldset id="totals">
        <legend>Totals</legend>

        <div id="total_food" class="subtotal">
          <p>Food</p>

          <label for="total_racks_per_storey">
            Racks per Storey
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_racks_per_storey" name="total_racks_per_storey" />

          <label for="total_plants_per_building">
            Plants per Building
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_plants_per_building" name="total_plants_per_building" />

          <label for="total_energy_per_plant">
            Energy per Plant (kJ)
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_energy_per_plant" name="total_energy_per_plant" />

          <label for="total_energy_per_harvest">
            Energy per Harvest (kJ)
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_energy_per_harvest" name="total_energy_per_harvest" />

          <label for="total_energy_production">
            Annual Energy Production (kJ)
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_energy_production" name="total_energy_production" />

          <label for="total_energy_consumption">
            Annual Energy Consumption (kJ)
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_energy_consumption" name="total_energy_consumption" />
        </div>

        <div id="total_electricity" class="subtotal">
          <p>Illumination</p>

          <label for="total_area">
            Crop Area (m<sup>2</sup>)
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_area" name="total_area" />

          <label for="total_lamps">
            Lamps
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_lamps" name="total_lamps" />

          <label for="lamps_power">
            Lamps Power (W)
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="lamps_power" name="lamps_power" />

          <label for="total_conveyers">
            Conveyers
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_conveyers" name="total_conveyers" />

          <label for="conveyers_power">
            Conveyers Power (W)
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="conveyers_power" name="conveyers_power" />

          <label for="total_electric_consumption">
            Total Power Consumption (W)
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_electric_consumption" name="total_electric_consumption" />

          <label for="total_electric_cost">
            Annual Energy Cost ($)
          </label>
          <input
            type="text" value="" readonly="readonly"
            id="total_electric_cost" name="total_electric_cost" />
        </div>
      </fieldset>
    </form>
  </xsl:template>

  <!-- Replace superscript encoded as ^# with <sup>#</sup>. -->
  <xsl:template match="unit">
    <xsl:value-of
      select="replace( ., '\^(\d)', '&lt;sup>$1&lt;/sup>' )"
      disable-output-escaping="yes" />
  </xsl:template>

  <xsl:template match="farm/crop/name">
		<xsl:apply-templates />
  </xsl:template>
</xsl:stylesheet>

