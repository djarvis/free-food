<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output omit-xml-declaration="yes" />
  <xsl:output method="html" indent="no"/> 
  <xsl:strip-space elements="*"/>

  <xsl:param name="minify" />
 
  <xsl:template match="/">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
    <html lang="en">
      <head>
        <xsl:apply-templates mode="head" />
      </head>
      <body>
        <div class="page">
          <header class="section header">
            <xsl:apply-templates mode="header" />
          </header>
          <nav class="section menu">
            <xsl:apply-templates mode="menu" />
          </nav>
          <main class="section content">
            <xsl:apply-templates mode="content" />
          </main>
          <footer class="section footer">
            <xsl:apply-templates mode="footer" />
          </footer>
        </div>
        <xsl:apply-templates select="." mode="scripts" />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="/root" mode="head">
    <meta
      name="viewport"
      content="width=device-width,initial-scale=1user-scalable=yes" />
    <meta
      http-equiv="X-UA-Compatible"
      content="IE=edge" />
    <meta
      name="description"
      content="A hard sci-fi novel about automated food production, sentient machines, and surveillance societies." />

		<link
      rel="icon" type="image/png" sizes="16x16"
      href="/favicon/favicon-16x16.png" />
		<link
      rel="icon" type="image/png" sizes="32x32"
      href="/favicon/favicon-32x32.png" />

    <xsl:apply-templates select="book/title" mode="head" />

    <xsl:apply-templates select="." mode="normalize" />
    <xsl:apply-templates select="." mode="styles" />
    <xsl:apply-templates select="." mode="fonts" />
  </xsl:template>

  <xsl:template match="/root" mode="menu" />

  <xsl:template match="/root" mode="header">
    <xsl:apply-templates select="book/title" mode="header" />
  </xsl:template>

  <xsl:template match="/root" mode="content">
    <div class="illustration">
      <img
        width="408" height="359"
        src="images/farmer.jpg" alt="Android Farmer" />
    </div>
    <p class="quote">
      <xsl:apply-templates select="c/protagonist/speech/tagline" mode="content" />
      <xsl:text> ~ </xsl:text>
      <xsl:apply-templates select="c/protagonist/name" mode="content" />
      <xsl:text>, 2058.</xsl:text>
    </p>
  </xsl:template>

  <xsl:template match="/root" mode="footer" />

  <xsl:template match="/root" mode="normalize">
    <style media="screen" type="text/css">
/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,footer,header,nav,section{display:block}h1{font-size:2em;margin:.67em 0}figcaption,figure,main{display:block}figure{margin:1em 40px}hr{box-sizing:content-box;height:0;overflow:visible}pre{font-family:monospace,monospace;font-size:1em}a{background-color:transparent;-webkit-text-decoration-skip:objects}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:inherit}b,strong{font-weight:bolder}code,kbd,samp{font-family:monospace,monospace;font-size:1em}dfn{font-style:italic}mark{background-color:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}audio,video{display:inline-block}audio:not([controls]){display:none;height:0}img{border-style:none}svg:not(:root){overflow:hidden}button,input,optgroup,select,textarea{font-family:sans-serif;font-size:100%;line-height:1.15;margin:0}button,input{overflow:visible}button,select{text-transform:none}[type=reset],[type=submit],button,html [type=button]{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{border-style:none;padding:0}[type=button]:-moz-focusring,[type=reset]:-moz-focusring,[type=submit]:-moz-focusring,button:-moz-focusring{outline:1px dotted ButtonText}fieldset{padding:.35em .75em .625em}legend{box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{display:inline-block;vertical-align:baseline}textarea{overflow:auto}[type=checkbox],[type=radio]{box-sizing:border-box;padding:0}[type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}[type=search]::-webkit-search-cancel-button,[type=search]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}details,menu{display:block}summary{display:list-item}canvas{display:inline-block}template{display:none}[hidden]{display:none}/*# sourceMappingURL=normalize.min.css.map */
    </style>
  </xsl:template>

  <xsl:template match="/root" mode="styles">
    <xsl:call-template name="stylesheet">
      <xsl:with-param name="file" select="'simple'" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="/root" mode="fonts">
    <link
      href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200|Libre+Baskerville"
      rel="stylesheet" />
  </xsl:template>

  <xsl:template match="/root" mode="scripts" />

  <xsl:template match="book/title" mode="head">
    <title><xsl:apply-templates /></title>
  </xsl:template>

  <xsl:template match="book/title" mode="header">
    <h1><xsl:apply-templates /></h1>
  </xsl:template>

  <xsl:template match="tagline" mode="content">
    <q><xsl:apply-templates /></q>
  </xsl:template>

  <xsl:template match="protagonist/name" mode="content">
    <xsl:value-of select="First" />
    <xsl:text> </xsl:text>
    <xsl:value-of select="Family" />
  </xsl:template>

	<!-- Includes a stylesheet, optionally minified. -->
  <xsl:template name="stylesheet">
    <xsl:param name="file" />
		<xsl:param name="relative" select="''" />

    <link
      rel="stylesheet" type="text/css" media="screen">
      <xsl:attribute name="href">
        <xsl:value-of select="$relative" />
        <xsl:text>themes/</xsl:text>
        <xsl:value-of select="$file" />
        <xsl:value-of select="$minify" />
        <xsl:text>.css</xsl:text>
      </xsl:attribute>
    </link>
  </xsl:template>

	<!-- Includes a script, optionally minified. -->
  <xsl:template name="script">
    <xsl:param name="file" />

    <script defer="defer">
      <xsl:attribute name="src">
        <xsl:value-of select="$file" />
        <xsl:value-of select="$minify" />
        <xsl:text>.js</xsl:text>
      </xsl:attribute>
    </script>
  </xsl:template>

  <xsl:template match="*" />

</xsl:stylesheet>

