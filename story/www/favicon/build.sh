#!/bin/bash

# Relative path to location of SVG file to make into ICO file.
ICON_PATH=../../images/edible.svg

ICON_BASE=$(basename "$ICON_PATH")
ICON_DIR=$(dirname "$ICON_PATH")
ICON_FILE="${ICON_BASE%*.}"
ICON_EXT="${ICON_BASE##*.}"

FAVICON_FILE=favicon
FAVICON_EXT=.ico

# This uses rsvg-convert to create crisp PNG icons.
for size in 16 32 64 128 150 192 512; do
  ICON_OUT=$ICON_FILE-${size}.png
  DIMENSIONS=${size}x${size}

  echo "Create ${ICON_OUT}"

  #rsvg-convert -w $size -p 300 -d 300 $ICON_PATH > $ICON_OUT
  inkscape \
    --batch-process \
    -d 300 \
    -w $size \
    --export-type=png \
     -o $ICON_OUT \
    $ICON_PATH

  # Center the image and make it square.
  convert $ICON_OUT -gravity center -background transparent \
    -resize $DIMENSIONS -extent $DIMENSIONS temp-$ICON_OUT

  # Use 8-bit colour to reduce the file size.
  pngquant 256 < temp-$ICON_OUT > $FAVICON_FILE-$DIMENSIONS.png
done

# Create a multi-sized ICO file.
convert \
  -resize x16 \
  -gravity center \
  -crop 16x16+0+0 \
  -flatten \
  -colors 256 \
  -background transparent \
  -define icon:auto-resize=32,16 \
  $FAVICON_FILE-512x512.png \
  ../$FAVICON_FILE$FAVICON_EXT

# Create Android icons.
mv $FAVICON_FILE-192x192.png android-chrome-192x192.png
mv $FAVICON_FILE-512x512.png android-chrome-512x512.png

# Create MS tile icon.
mv $FAVICON_FILE-150x150.png mstile-150x150.png

# Clean up the temporary files.
rm ${ICON_FILE}*png temp-${ICON_FILE}*png

