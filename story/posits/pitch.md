In 2042, a girl envisions robots supplanting human labour in food production
(using renewable energy). Her plans are sabotaged by a kafkaesque
surveillance government because free food would undermine societal controls
and destroy their capitalistic economy.

The work bridges several topics, including: justice, technology, sentience,
food, consumerism, governance, and environmental issues.

The story is written for an audience who enjoys science-fiction. They will
care because the story addresses issues that concern us all: dwindling job
market due to automation, surveillance, robots, time, and freedom.

John Payne (of RoboHub) pushed the story through text-to-speech synthesis.
He wrote, "I just finished *listening* to the most recent version of
Convergence, and am still reverberating from the experience.... it was...
powerful..." He also wrote, "I found it to be a page-turner, with some
interesting characters and a plot that I might go out on a limb and call
captivating."

I am a software developer by trade -- I have no experience in the production
of script writing, nor organising people to produce an audio drama. I would
need help with all aspects of production, including finding voice actors
to play the various roles. The story has four major characters, five minor
characters, and three bit parts. I have little experience writing
[screenplays](https://bitbucket.org/djarvis/free-food/raw/master/story/screenplay/convergence.pdf).

Convergence makes extensive use of flowing alliteration. Consider these few
phrases, "machines that were forged in the flames of the Industrial
Revolution," "a scant six minutes to surmise that she would destabilise,"
"people in positions of power to know where and when and with whom we met,"
"the clone could not have committed that very crime ater its creation,"
"your democratic dream, that I pay to subvert, cannot displace my money
machine," and "but enough to begin a rebellion."

The novelette (13,500 words) has been in the works since May. There are no
audio segments to provide a sense of style. I imagine that the story could
be easily converted into script format almost verbatim. The story is narrated
from the point of view of two of the four major characters.

You may [download](https://bytebucket.org/djarvis/free-food/raw/master/story/convergence.pdf) the story at your leisure.

iTunes would compare it to shows and movies such as: Star Trek: The Next
Generation, Doctor Who, Ex Machina, Her, Minority Report, Forbidden Planet,
Metropolis, Wall-E, and Brazil.

