In 2042, a girl envisions robots supplanting human labour in food production using renewable energies. Her plans are sabotaged by a Kafkaesque surveillance government because free food would undermine societal controls and destroy their capitalistic economy.

The story intermixes surveillance, self-aware machines, war, technology, and humanity's common foe: hunger. This purely speculative science-fiction would work best as a podcast.

