# Food Economy

Thoughts stimulated from a discussion of free food with an economist. Here, *free* means neither money nor human time are expended.

## On Starvation

**"Starvation slows population growth."**

Cruelty notwithstanding, population can be reduced using a multi-pronged approach. Foremost, ensure that all girls and women have unrestricted access to education. Next, provide our youth with comprehensive, age-appropriate sexuality education. Make available unrestricted access to inexpensive contraceptives for everyone. Lastly, governments should end financial incentives or assistance for families with multiple children.

**"Mao enforced self-sufficiency by brandishing starvation."**

Famines are caused by inefficient food distribution. Great dynasties and empires relied upon crops grown and cultivated on vast plains of arable land, but we have no such constraints. We can mix and match crops locally to process and distribute on regional and municipal levels. This addresses food security, distribution, and self-reliance in ways that Mao could never have foreseen.

---

## On Efficiency

**"Efficiency gains lacking distribution can have negative outcomes."**

Distribution will be wholly automated and delivered, potentially, direct to individuals, regardless of location or logistics. Vertically scaled indoor farms can produce enough produce to provide balanced meals to populations using a fraction of traditional agriculture's land usage. Synthetic meat will further liberate land allocated to animal crops, allowing the land to be reclaimed by nature.

Population is not linked to food alone and can be curtailed with the aforementioned controls. Other negative outcomes can likely be addressed through education.

**"Workers will be displaced."**

Well-fed people can self-organize to apply their idle time into building more indoor farms. It's like shipping railroad construction materials--such as ties and rails and I-beams--along the very railway they are meant to extend. Each farm frees up the community to build an indoor farm for the next neighbourhood because relaxing economic constraints naturally leads to increased co-operation.

---

## On Labour

**"What distinguishes agrarian labour from other labour?"**

Increased agricultural labour transitions the agricultural workforce from food production into production of other goods and services, over time. As agricultural labour productivity increases, it lowers the cost of food availability per worker, which results in higher incomes that then spurs demand for non-food goods and services. This makes agricultural labour a keystone within the broader economy.

Hunger is a tempermental river that flows relentlessly towards immediacy. Food differs from other goods and services in that it puts the potential for covert, or overt, tyrannical power into too few hands. Whomever controls the food, controls the nation. Chairman Mao showed that this can have ghastly fatal consequences. Without an engine to drive food into our mouths, billions of people would die from starvation. Few industries have such a singular importance.

Knowledge was once weaponised to command the masses. Today, food has taken the place of knowledge as the instrument of control. Knowledge was freed by the printing press to the enlightenment of humanity. What equivalent have we for food?

---

## On Society

**"Free food may lead to an unanchored lifestyle."**

It does not follow from free food that manually grown food shall be prohibited. We can tend gardens, conquer mountains, walk within bamboo forests, or swim 'twixt tangles of kelp in the seas. We can anchor ourselves to nature in many ways. Still, if humanity needed anchoring to the Earth, why would we desire to reach for the stars? But we do. And we have, long before telescopes pointed skyward. We are going to explore planets, lifeless rocks, where the only Earthly tethers will be DNA sequences stored aboard the databanks of our spacecraft, waiting to be reborn through printed gene synthesis.

**"Village movements return to an eco-friendly lifestyle."**

If cities fell in favour of small, close-knit eco-villages, it would slow our technological progress. Simpler lifestyles, while wholesome, reap our time in spades, leaving meagre scraps for other ventures. Small communities focused on sustainable food production would ground their own lives. Economies of scale are not feasible by villages.

---

## On Value

**"A diamond can be more valuable than water."**

Water is universally plentiful: it falls from the sky, it pools into lakes, and flows freely when it melts from mountain tops. Our primordial planet's name should have dipped its etymological roots in watōr, not erþō. Food never collects itself in barrels without helping hands--unlike the rain. Food, while plentiful, is not evenly distributed. Even if it was, agriculture causes nearly a quarter of all greenhouse gas emissions, and compensation necessary for the time toiled by workers along every link of the food chain.

Some of us desire diamonds, but we all need food.

---

## On Alternatives

**"We could have a chit for free food."**

Food stamps, similar to free food chits, are vexed with problems. Millions of dollars are lost to fraudulent schemes. Administrative costs stagger into the billions. Chits would have similar economic overhead and would be exchanged for contraband and weapons. Ignoring those issues, who pays for the food? Human involvement can be addressed in a few ways: volunteers, tax hikes, or donations. Volunteers from planting through harvesting to delivery is feasible neither logistically nor economically. More tax presses an unfair burden upon those who were fortunate enough to avoid automation's unemployment boom. Donations are neither short- nor long-term sustainable.

Though automation creates new employment opportunities, the replacement rate is lower than the elimination rate--and that's assuming an educational infrastructure to retrain everyone for higher labour exists, which it doesn't. A general-purpose robot can replace ten human workers for pennies' worth of electricity. What's worse: newly created jobs can themselves be more easily automated than those that came before.

Chits, like mincome, would be a stop-gap, at best, and a distraction from a long-term solution at worst.

