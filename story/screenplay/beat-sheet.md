# Beat Sheet

## Scene One

Earth seen from space. Zoom to young girl reaching for an apple. Apple is
out of reach, hanging from a tree.

Cross-fade

## Scene Two

Montage:

* Apple orchard. People (tired, dirty, run-down) hand-picking food.
* Farmer driving combine through a field, billowing smoke spewing.
* Inside food processing factory, grungy, apples bumped and bruised, no
workers.
* Food production facilities from all over the world.

Montage speeds up to chaos, audio increases. Abrupt silence. Black.

**Narrator**: What if we had more time for each other?

## Scene Three

Int. Kitchen.

Two small families sharing a meal, discussing ideas. Vertical farm blueprints
on counter. 

## Scene Four

Montage:

* People living in skyscrapers
* Pollution (e.g., China smog)

Versus

Montage:

* Vertical farms
* Clean skyline, no pollution, lots of trees
* [Video 1](https://www.youtube.com/watch?v=8UgP1ithEFU)
* [Video 2](https://www.youtube.com/watch?v=Ct3dK2_ksvk)

(TODO: Research - South Korea or Germany or Arizona, various heights and stages
of automation.)

## Scene Five

Fly-over in order:

1. people leaving grocery store; to
1. parking lot; to
1. roadways (people driving); to
1. gridlocked traffic; to
1. people arriving home.

**Narrator**: What if food came to you?

Cross-fade

Self-driving electric vehicles on vacant streets.

## Scene Six

Int. Vertical farm.

Plants and automatons. Computer systems, cool, and clean, nutrient
and water distribution. No people.

**Narrator**: What if we had more time to grow?

## Scene Seven

Montage:

* Grandparents gardening with grandchildren
* Artistic endeavours
* Helping the homeless
* Scientific advancements

## Scene Eight

Young girl takes apple from robotic hand.

**Narrator**: What if food was free?

