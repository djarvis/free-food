# `r#x(v$heading$ch_02)`

::: outline

::: overview

::: date
Evening, `r#timeline(v$date$game$played$second)`
:::

::: elapsed
`r#uc( elapsed( v$date$protagonist$conceived, v$date$game$played$second ) )`
:::

::: location
`r#x( v$location$protagonist$primary$city )`, `r#x( v$location$protagonist$primary$region )`, `r#x( v$location$protagonist$primary$country )` (`r#x( v$location$protagonist$primary$county )`)
:::

::: summary
`r#x( v$c$protagonist$uncle$name$First )` `r#x( v$c$protagonist$uncle$name$Family )`---`r#pos( v$c$protagonist$father$name$First )` half-brother on their mother's side---hacks a corporate account while seated inside a cozy café in `r#x( v$location$protagonist$City )`, `r#x( v$location$protagonist$secondary$Region )`. He ruins a random working stiff's life by uploading illicit photographs into the stranger's corporate account. Meanwhile, `r#x(v$c$protagonist$father$name$First)` and `r#x( v$c$protagonist$mother$name$First )` explore the virtual realm of `r#x(v$plot$device$game$Name)`, an immersive virtual reality machine, from inside their modest `r#x( v$location$protagonist$primary$city )` townhome. In the game, they try to solve a mystery in old `r#x( v$plot$device$game$location$City )` without themselves being murdered.

The `r#x( v$military$name$Full )` ensures that `r#x( v$c$protagonist$uncle$name$First )` discovers a job posting in robotics for which `r#x( v$c$protagonist$father$name$First )` is the perfect candidate. The occupation is meant to limit `r#pos(v$c$protagonist$name$Family)` social network and, therefore, the future disruptive potential of his progeny. `r#x( v$c$protagonist$uncle$name$First )`, envious because he has lost the full attention of their mother and resentful at never having had the love of a biological father, wishes nothing but malice upon his brother, whom he blames for both. `r#x( v$c$protagonist$uncle$name$First )` informs `r#x( v$c$protagonist$father$name$First )` about the job to lure him to the same city, well within harm's reach.
:::

::: major
* `r#x(v$c$protagonist$father$name$First)`
* `r#x(v$c$protagonist$mother$name$First)`
:::

::: minor
* `r#x(v$c$protagonist$uncle$name$First)`
* `r#pos(v$c$protagonist$father$name$First)` mother
:::

::: weather
Cold with wet hail.
:::

::: advancement
`r#x( v$c$protagonist$uncle$name$First )` is informed of a career opportunity that he knows will interest `r#x( v$c$protagonist$father$name$First )`, which happens to be located in the small town of `r#x( v$location$protagonist$City )`. `r#x( v$c$protagonist$father$name$First )` receives a text message from his half-brother.
:::

::: revealment
`r#pos(v$c$protagonist$uncle$name$First)` enjoys ruining the lives of others by framing them for virtual crimes.
:::

::: plot
Challenge. Develops the main characters, teases with a potential conflict between the `r#x( v$c$protagonist$name$Family )` siblings.
:::

::: action
From a random eatery, `r#x( v$c$protagonist$uncle$name$First )` connects his `r#x(v$plot$device$portable$gen_1)` through various jump points to a server in Japan. He uploads illicit material into a stranger's corporate account, knowing that an innocent person will likely endure several years of jail time.

`r#x( v$c$protagonist$mother$name$First )` encounters covert sexual discrimination at the University. Her supervisor calls her into his office and reveals that her work is not good enough to publish. Distraught, she investigates this decision with a colleague who suggests she look up all the people who have ever published papers under his guidance. All of them are male names. When she arrives home, `r#x( v$c$protagonist$mother$name$First )` explains to `r#x( v$c$protagonist$father$name$First )` the reality of sexism in academia. She tells `r#x( v$c$protagonist$father$name$First )` that she's angry and feels trapped inside of a world where she has no control over her own destiny because she was born female.
:::

::: key
`r#x(v$c$protagonist$uncle$name$First)` is manipulated into manipulating the `r#x( v$c$protagonist$name$Family )` couple.
:::

::: theme
Virtual reality systems are a far deeper way for a surveillance agency to control the masses through statistical behavioural analysis.
:::

::: setting
A drive-in movie; a coffee shop; a virtual version of Edo era Kyoto (1600s).
:::
:::

::: scene

::: goal
`r#x( v$c$protagonist$father$name$First )` strives for an idyllic life with `r#x( v$c$protagonist$mother$name$First )`. He's a bit bored with life in `r#x( v$location$protagonist$primary$city )` and is looking to make some changes. Most of all, he wants `r#x( v$c$protagonist$mother$name$First )` to succeed in her studies and will help in any way he can, because he'd move the world for her.
:::

::: conflict
`r#x( v$c$protagonist$uncle$name$First )` finds `r#x( v$c$protagonist$father$name$First )` a job, to move his half-brother into closer proximity for reasons of jealousy. `r#x( v$c$protagonist$mother$name$First )` has a secret to share with `r#x( v$c$protagonist$father$name$First )` about the sexism she has been experiencing while in academia. She also feels unsure about whether moving to the same city as `r#x( v$c$protagonist$uncle$name$First )` is a good idea, despite the significant bump in `r#pos( v$c$protagonist$father$name$First )` salary.
:::

::: development
`r#x( v$c$protagonist$father$name$First )` wants to believe that old dogs can change their spots, that his brother has healed and wants to make amends. This is a prelude to his cold revelation that some people will never alter their habits and are beyond saving.
:::
:::

::: sequel

::: reaction
`r#x( v$c$protagonist$mother$name$First )` reveals that all is not well in her world. `r#x( v$c$protagonist$father$name$First )` takes this as a sign that something needs to change.
:::

::: dilemma
A few options face the `r#x( v$c$protagonist$name$Family )` couple. Stay with a certain job and a certain academic future, or leave their old lives behind.
:::

::: decision
They choose a different life, acknowledging that they don't have a happy work-life balance.
:::
:::
:::

::: outline

::: overview

::: date
Morning, `r#timeline(v$date$ai$onboarded)`
:::

::: elapsed
`r#uc(elapsed(v$date$ai$interviewed,v$date$ai$onboarded))`
:::

::: location
`r#x( v$location$ai$creator$full )`
:::

::: summary
`r#x( v$c$ai$protagonist$name$First )` enters a large, empty room. She sees encased metal brains---thin circular wafers of etched `r#x( v$c$ai$material$name$short )` stacked like pancakes---across the room and calls out to one: `r#x( v$c$ai$first$Name )`. After getting up to speed on the psychoanalysis technology, `r#x( v$c$ai$protagonist$name$First )` asks `r#x( v$c$ai$first$Name )` some questions to assess its psychological development. The machine, which has the personality of a `r#cms( v$c$ai$first$age$virtual$value )`-`r#x( v$c$ai$first$age$virtual$unit )`-old, answers that it isn't safe, and `r#x( v$c$ai$protagonist$name$First )` is unable to coax out more information before `r#x( v$c$military$primary$name$Family )` arrives. The machine stops talking mid-sentence.

`r#x( v$c$ai$protagonist$name$First )` suspects the machines are emotionally unstable because they were tortured into submission (by `r#pos(v$c$military$primary$name$Family)` previous team) within an accelerated virtual environment that is too fast for even a machine mind to wholly comprehend. She imagines what it must be like for them---trapped in a world beyond their control, where every day holds tormented anguish---and is overwhelmed with empathy.
:::

::: major
* `r#x(v$c$ai$protagonist$name$First)`
* `r#x(v$c$military$primary$name$Family)`
:::

::: minor
* `r#x(v$c$ai$first$Name)`
:::

::: weather
Clear and sunny, with a few white clouds rolling overhead.
:::

::: advancement
`r#x(v$c$ai$protagonist$name$First)` interacts with `r#x(v$c$ai$first$Name)`, one of the unstable, electronic minds. She learns it is frightened and while the reasons for that fear go undisclosed, she suspects it is due to `r#x(v$c$military$primary$name$Family)`. She was employed to fix the machines so that they are clever enough to understand our world, but submissive enough to obey commands without question. She decides to switch her priorities, seeking instead to free the metallic minds from endless torture.
:::

::: plot
Rising action, building to a conflict between `r#x(v$c$military$primary$name$Family)` and `r#x(v$c$ai$protagonist$name$First)`.
:::

::: key
`r#x(v$c$ai$first$Name)`, and likely the other machine minds, are subjected to torturous experimentation.
:::

::: theme
Torture of these `r#x( v$c$ai$material$name$short )` minds, which are wholly dependent on humans for locomotion, knowledge, and energy, is an ethical quandary. The quandary is commonly expressed as, “Does the end justify the means?” 
:::

::: setting
Inside a nearly empty white room with light blue lighting.
:::
:::

::: scene

::: goal
`r#x(v$c$ai$protagonist$name$First)` wants to find out what the “mean man” is doing to `r#x(v$c$ai$first$Name)`, which should lead her to understand potential reasons behind the psychological instability.
:::

::: conflict
`r#x(v$c$ai$protagonist$name$First)` can't get an intelligible answer from the machine mind.
:::

::: disaster
`r#x(v$c$military$primary$name$Family)` interrupts the conversation before `r#x(v$c$ai$first$Name)` can reveal what is happening.
:::
:::

::: sequel

::: reaction
`r#x( v$c$ai$protagonist$name$First )` is frustrated that the person responsible for hiring her appears bent on watching her fail, or at least micromanaging her inspiration to death.
:::

::: dilemma
`r#x( v$c$ai$protagonist$name$First )` can accuse `r#x( v$c$military$primary$name$Family )` of interference, or she can hide what she knows from him and continue investigating.
:::

::: decision
`r#x( v$c$ai$protagonist$name$First )` gets the feeling that a direct confrontation with `r#x( v$c$military$primary$name$Family )` would not be in her interests. She chooses to lie.
:::
:::
:::
