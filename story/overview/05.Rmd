# `r#x(v$heading$ch_05)`

::: outline

::: overview

::: date
Late morning, `r#timeline(v$date$protagonist$idea)`
:::

::: elapsed
`r#uc( elapsed( v$date$protagonist$attacked$second, v$date$protagonist$idea ) )`
:::

::: location
`r#x( v$location$protagonist$origin )`
:::

::: summary
`r#x( v$c$protagonist$name$First )` (`r#cms(years(v$date$protagonist$born, v$date$protagonist$idea ))` years old)  and `r#x( v$c$protagonist$mother$name$First )` are in a greenhouse, tending the potted plants to grow food to save money for `r#pos(v$c$protagonist$name$First)` future education. The funds have been depleted by hospital bills, the car accident, and other incidentals.
They have left their `r#pl( v$plot$device$phone$name )` behind; nobody can eavesdrop on their conversation. `r#x( v$c$protagonist$name$First )` mentions her suspicion that her mother's feelings towards `r#x( v$c$protagonist$friend$primary$mother$name$First )` `r#x(v$c$protagonist$friend$primary$name$Family)` are deeper than platonic. Her mother protests, but `r#x( v$c$protagonist$name$First )` reminds her that she's no longer a little girl and can be a confidante. With some trepidation, `r#x(v$c$protagonist$mother$name$First)` opens up. `r#x( v$c$protagonist$name$First )` learns to question everything.

`r#pos( v$c$protagonist$mother$name$First )` impassioned speech inspires `r#x( v$c$protagonist$name$First )` to imagine how technology could enable a new world for humanity. A world where nobody needs to dig through the dirt for food or be crushed by monetary debt.
:::

::: major
* `r#x( v$c$protagonist$mother$name$First )`
* `r#x(v$c$protagonist$name$First)` (`r#cms(years(v$date$protagonist$born,v$date$protagonist$idea))`)
:::

::: growth
`r#x( v$c$protagonist$name$First )` and `r#x( v$c$protagonist$mother$name$First )` grow closer as `r#x( v$c$protagonist$mother$name$First )` shares a secret with her daughter.
:::

::: weather
Hot, humid, and a little bit smoggy. The average seasonal temperature has been increasing over the last few decades, slowed but not stopped or reversed.
:::

::: advancement
This is the moment when `r#x( v$c$protagonist$name$First )` realizes that our entire food system can be automated, from planting to harvest to processing to delivery. She gets an image in her mind about how the entire system could work.
:::

::: revealment
`r#x( v$c$protagonist$mother$name$First )` has a crush on `r#x( v$c$protagonist$friend$primary$father$name$First )`, but has kept it a secret from `r#x( v$c$protagonist$father$name$First )` for fear that he'd misunderstand her intentions.
:::

::: plot
Revelation. `r#x( v$c$protagonist$name$First )` realizes that there's a different way to run the world.
:::

::: action
`r#x( v$c$protagonist$name$First )` and `r#x( v$c$protagonist$mother$name$First )` come into conflict about `r#pos( v$c$protagonist$name$First )` maturity. `r#x( v$c$protagonist$mother$name$First )` has an urge to withhold information, but `r#x( v$c$protagonist$name$First )` is determined to find out.
:::

::: key
`r#x( v$c$protagonist$name$First )` states that we shouldn't bow to tomatoes, which hints that most of humanity is leading a manufactured life of indentured slavery.
:::

::: theme
Food weaves its way throughout the book as one of the main themes. The automation of food is the problem that `r#x( v$c$protagonist$name$First )` dedicates her life to solving.
:::

::: setting
A greenhouse.
:::
:::

::: scene

::: goal
`r#x( v$c$protagonist$mother$name$First )` wants `r#x( v$c$protagonist$name$First )` to learn how to garden, to care for growing food as a means to means.
:::

::: conflict
`r#x( v$c$protagonist$name$First )` decides that growing food manually is a waste of time and it should be entirely automated.
:::

::: disaster
`r#x( v$c$protagonist$mother$name$First )` wants `r#x( v$c$protagonist$name$First )` to learn about caring for the Earth by caring for plants.
:::

::: failure
`r#x( v$c$protagonist$name$First )` decides that caring for plants is a waste of time, but doesn't realize that `r#x( v$c$protagonist$mother$name$First )` mistakes her disinterest in caring for plants as a lack of care for the world. `r#x( v$c$protagonist$name$First )` cares deeply.
:::

::: development
Some distance grows between `r#x( v$c$protagonist$mother$name$First )` and `r#x( v$c$protagonist$name$First )`, partly due to miscommunication, but also as `r#x( v$c$protagonist$name$First )` stretches her wings and pushes the boundaries set upon her by her parents.
:::
:::

::: sequel

::: reaction
`r#x( v$c$protagonist$name$First )` begins to resent the time `r#x( v$c$protagonist$mother$name$First )` wants to spend gardening. `r#x( v$c$protagonist$name$First )` feels like the ordeal is a waste of time. `r#x( v$c$protagonist$mother$name$First )` is confused about how to cross the chasm that is forming between them.
:::

::: dilemma
Grounding her daughter will only exacerbate the problem. Everything she says to her daughter seems to be misinterpreted. Talking with `r#x( v$c$protagonist$father$name$First )` about it---without `r#x( v$c$protagonist$name$First )` present---would feel like driving a blade between father and daughter.
:::

::: decision
Instead, `r#x( v$c$protagonist$mother$name$First )` opts to take `r#x( v$c$protagonist$name$First )` in for a counselling session.
:::
:::
:::

::: outline

::: overview

::: date
Afternoon, `r#timeline(v$date$ai$trapped)`
:::

::: elapsed
`r#uc(elapsed(v$date$ai$resigned,v$date$ai$trapped))`
:::

::: location
`r#x( v$location$ai$creator$full )`
:::

::: summary
`r#x( v$c$ai$protagonist$name$First )` has a dream in which she finds a small horde of items to use for her escape. Upon waking, she heads to one of the storage closets on `r#x( v$military$compound$type )`, convinces a cloudy-eyed desk clerk (doped up on a `r#x(v$plot$device$biotech$stimulant$short)`) to unlock the door, and is trapped inside with toxic fumes. Eventually, the door opens, and she runs through empty corridors to meet `r#x( v$c$military$primary$name$Family )` in the lab where the metal minds reside.

`r#x( v$c$military$primary$name$Family )` is unimpressed with her progress, and plans to requisition a new team to do what neither the old team nor `r#x( v$c$ai$protagonist$name$First )` has managed. While talking, `r#x( v$c$military$primary$name$Family )` relents and allows her to use a previous version of `r#x( v$c$ai$fourth$Name )` for her psychoanalysis. After `r#x( v$c$military$primary$name$Family )` leaves the lab, `r#x( v$c$ai$protagonist$name$First )` purges the current version of `r#x( v$c$ai$fourth$Name )`, notching another death, and further chipping away at her sense of morality.

After work, she jogs her evening route, stopping only briefly to perform a bit of chemistry beside a shed---one of the few outdoor locations unsurveilled by either automaton sentries or exterior cameras. She worries that her absence from the `r#pl( v$plot$device$video$name )` will also raise alarms.
:::

::: major
* `r#x(v$c$ai$protagonist$name$First)`
* `r#x(v$c$military$primary$name$Family)`
:::

::: minor
* `r#x(v$c$ai$first$Name)` (purged)
* `r#x(v$c$ai$second$Name)` (purged)
* `r#x( v$c$ai$third$Name )` (referenced)
* Desk Clerk
:::

::: growth
`r#x( v$c$ai$protagonist$name$First )` begins to despise her environment. She channels her anger into concrete actions to escape. While she knew in advance that part of the job might have included destroying sapient life, she misjudged her capacity to carry out the orders with a cold distance. She grew to like the machine personalities, and it tears her apart to purge them.
:::

::: weather
Extremely hot, muggy. Skies are overcast, but no rain falls, no break from the heat in sight.
:::

::: advancement
`r#x( v$c$ai$protagonist$name$First )` realizes that even if she escapes, the machine minds she has been experimenting on will likely be subjected to endless torture. She vows to destroy the brains to end their suffering.
:::

::: plot
Rising action.
:::

::: action
`r#x( v$c$ai$protagonist$name$First )` gets trapped inside a storage room brimming with toxic fumes. When she next encounters `r#x( v$c$military$primary$name$Family )`, he orders her to start over with `r#x( v$c$ai$second$Name )`, the second brain.
:::

::: key
`r#x( v$c$ai$protagonist$name$First )` left the spray bottle filled with water.
:::

::: theme
One of the themes relates to the morality of killing sophont machines, regardless of whether they are armed. Sophont machines are a two-edged sword, and where we walk the ethical line with our enslavement of machines for our own mundane well-being or offensive attacks is a difficult path.
:::

::: setting
The scene takes place in a lab and outside beside a tool shed.
:::
:::

::: scene

::: goal
`r#x( v$c$ai$protagonist$name$First )` plans to escape, preferably before she has to kill another machine.
:::

::: conflict
She gets trapped in a storage closet trying to retrieve chemicals she'll need to escape.
:::

::: disaster
First she gets locked into a closet, which makes her late with a meeting with `r#x( v$c$military$primary$name$Family )`. `r#x( v$c$military$primary$name$Family )` confronts her and orders her to kill another machine.
:::

::: failure
She purges the machine, killing a friend in the process, which weighs heavily on her conscious.
:::

::: development
Outwardly, she tries to convince herself that the killings are getting easier. Inwardly, however, she is being crushed into obedience, methodically.
:::
:::

::: sequel

::: reaction
`r#x( v$c$ai$protagonist$name$First )` feels stuck against the press of time. She feels trapped, angry, and regrets her decision to join the team.
:::

::: dilemma
To do her job, `r#x( v$c$ai$protagonist$name$First )` needs more information from `r#x( v$c$military$primary$name$Family )`, but talking with him is never smooth and seldom very informative. She's determined to escape and is worried that her plans will be discovered, or `r#x( v$c$military$primary$name$Family )` will sense her nervousness.
:::

::: decision
To avoid seeming unprofessional, she talks with `r#x( v$c$military$primary$name$Family )` about her progress.
:::
:::
:::
