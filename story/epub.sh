#!/usr/bin/env bash

# The purpose of this script is to generate an ePub version of the story.

FILENAME=$(cat title.txt).epub
CSS=epub.css
IMAGES_DIR=images

# Ensure images get embedded into the document.
for url in $(grep image $CSS | grep url | sed 's/.*url( *\(.*\) *).*/\1/g'); do
  EMBED="$EMBED --epub-embed-font=$IMAGES_DIR/$url"
done

# Generate the ePub, including fonts and images.
pandoc \
  --smart \
  --epub-cover-image=cover/cover.png \
  --epub-metadata=metadata.xml \
  --epub-stylesheet=$CSS \
  --epub-embed-font=fonts/MyUnderwood.ttf \
  $EMBED \
  -t epub -o $FILENAME output/*.vars

