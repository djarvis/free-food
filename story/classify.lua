function Div( element )
  local annotation = element.classes:find_if( matches )

  if annotation then
    annotation = annotation:gsub( "[^%w]*", "" )

    return {
      context( "\\start", annotation ),
      element,
      context( "\\stop", annotation )
    }
  end
end

function matches( s )
  return s:match( "^%a+" )
end

function context( macro, annotation )
  return pandoc.RawBlock( "context", macro .. "book" .. annotation )
end

-- Called to write information to a file.
log = function( s )
  local file = io.open( "classify.debug", "a+" )
  io.output( file )
  io.write( s )
  io.write( "\n" )
  io.close( file )
end

-- Called to write a Lua table to a file.
log_table = function( element, prefix )
	for i, v in pairs( element ) do
		if type( v ) == "table" then
			log_table( v, prefix .. "> " )
		else
			log( prefix .. type( i ) .. " " .. type( v ) .. " " .. i .. " = " .. v )
		end
	end
end

