<?xml version="1.0"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
  method="text"
  encoding="utf-8"
  omit-xml-declaration="yes"
  standalone="yes"
  indent="no"
  media-type="text/plain" /> 

<xsl:param name="chapter" />

<xsl:template match="/">
  <xsl:text>Chapter </xsl:text>
  <xsl:value-of select="$chapter" />
  <xsl:text>, </xsl:text>

  <xsl:apply-templates />
</xsl:template>

<xsl:template match="h1|h2">
  <xsl:apply-templates />
  <xsl:text>.</xsl:text>
  <xsl:text>&#x0a;</xsl:text>
</xsl:template>

<xsl:template match="h3">
  <xsl:text>By </xsl:text>
  <xsl:apply-templates />
  <xsl:text>.</xsl:text>
  <xsl:text>&#x0a;</xsl:text>
</xsl:template>

<xsl:template match="p">
  <xsl:apply-templates />
  <xsl:text>&#x0a;</xsl:text>
</xsl:template>

</xsl:stylesheet> 

