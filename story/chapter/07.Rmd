# `r#x(v$heading$ch_07)`

## Wherein revelations rise
### `r#x(v$narrator$one)` -- `r#annal(v$date$protagonist$surveilled)`

Trying to solve a seemingly inexplicable mystery upended my life. It started innocuously enough, when `r#x(v$c$protagonist$name$First)` sat beside me on the couch and said, "`r#x(v$c$protagonist$father$Endear)`, the `r#x(v$c$minor$quaternary$title)` uses up `r#pro.obj(v$c$minor$quaternary$sex)` entire lunch break, down to the minute; like, `r#pro.sub(v$c$minor$quaternary$sex)` probably ate quartz crystals as a kid. Why would `r#pro.sub(v$c$minor$quaternary$sex)` ever think to check the roof? Leopards and spots, right? `r#uc(pro.sub(v$c$minor$quaternary$sex))` doesn't even fart on a whim."

I raised a brow and glanced towards the basement door. "Police plus principal," I replied with a non-answer, half-thinking about ways I could retreat from this conversation into the `r#x(v$plot$device$game$Name)` cliffhanger.

"How'd the police know?"

I drew my eyes back to the e-report and read it aloud. "Officer `r#x(v$c$minor$tertiary$name$Family)` received an anonymous tip that two figures were on the roof of `r#x(v$location$protagonist$school$address)`. He relayed the information---"

"`r#x(v$c$protagonist$father$Endear)`, look. Nobody else was there. How would've anybody known? The school doesn't do `r#pl(v$plot$device$video$name)`. Besides, lying down while talking couldn't've tipped off any looky-loos or passersby from the street."

`r#x(v$plot$device$game$Name)` slipped from my thoughts. I said, "Maybe someone spotted your clandestine jaunt to the roof and you failed to notice? Or a microdrone flew by and its killjoy owner ratted. Or `r#x(v$c$protagonist$friend$primary$name$First)` was going to push you over the edge, so you made a preëmptive emergency call?"

"Impossible. No. And `r#x(v$c$protagonist$friend$primary$name$First)` is true."

"You really think that at lunch time in a school bustling with teach---"

`r#pos(v$c$protagonist$name$First)` `r#x(v$plot$device$phone$name)` vibrated.

"Or," she said, her synapses firing faster than mine, "tattle`r#x(v$plot$device$phone$name)`?"

Before she could answer the call, I snatched the device. "Confiscated, and consider yourself chastised. Ask to borrow your mother's `r#x(v$plot$device$portable$gen_1)` for the next week." I set my hand upon her shoulder, warmly. "`r#x(v$c$protagonist$name$First)`, if you're being tracked, we need to know."

She shrugged off my hand and glowered. "Give it back, now. Some files need to be ... organized."

I shifted position, shook my head, and gritted my teeth. "We talked about this, `r#x(v$c$protagonist$name$First)`. We warned you that `r#pl(v$plot$device$phone$name)` come with responsibilities and conditions. One condition being no nudes. No nudes. Two words. How was that not clear?" I pumped my volume a notch or four. "Why didn't you think? Did we raise an idiot for a daughter? Did we?"

`r#pos(v$c$protagonist$mother$name$First)` came steamrolling down the hallway.

"`r#uc(x(v$c$protagonist$name$nick$Mother))`," said `r#x(v$c$protagonist$mother$name$First)` using her quiet voice, "please, go to your room, lock your door, and listen to music until I knock, okay?" I felt the briefest twinge of an awkward silence between us while waiting until the door clicked closed. `r#x(v$c$protagonist$mother$name$First)` turned to face me with shards in her eyes and winter in her tone. "Downstairs."

In the basement, `r#x(v$c$protagonist$mother$name$First)` launched her rebuke. "Treat `r#x(v$c$protagonist$name$First)` like that again and we'll be signing off on custody arrangements." She pointed at the empty, crane-like cabinets. "You're gonna need to steady your keel because `r#x(v$plot$device$game$Name)` is tipping you overboard. Now answer me: How'd you bank them?"

I sat down in the nearest chair and lowered my forehead into my palms. "`r#x( v$c$protagonist$uncle$name$First )` and I developed an untraceable, peer-to-peer deliberation system that used unbreakable quantum cryptography and integrated a homomorphic database immune to side-channel attacks. Nation states can't touch it. We returned privacy to citizens.

"He said he had licensed our software---called *`r#x( v$plot$device$software$deliberation )`*---to a client in Mexico. No details. I didn't think to probe until I caught a few words in a note he was typing up and his `r#x( v$plot$device$phone$name )` buzzed with a number that I later looked up. Took some sleuthing to learn that our main breadwinner was a front runner for a cartel involved with human trafficking, black-market `r#pl(v$plot$device$biotech$stimulant$full)`, automatic---"

`r#pos(v$c$protagonist$mother$name$First)` nostrils flared. She stood, arms crossed, wearing a frown that rent my heart, and said nothing. After the smallest of head shakes, she turned away to climb the stairs.

"It was for the gays!" I blurted; she stopped climbing. "Do you recall Mexico's super-high homosexual homicide rates? Dissidents reinstated democratic governance and a proactive police force. Violent hate crimes against that demographic fell to nearly zip. The Saudi Insurrection? Eighteen million women granted legal personhood. Siberian anti-corruption movements? Two hundred thousand people pulled out of poverty.

"All `r#x( v$plot$device$software$deliberation )`-powered. He never told me the groups were penniless.

"`r#uc(cms(years(x(v$date$game$played$second) - 365,v$date$game$played$third)))` years ago, `r#x( v$c$protagonist$uncle$name$First )` split a cryptocurrency bonus from the cartel with me. I didn't know I bought the machines using blood money. When I finally confronted him, he dodged with a platitude about saving lives. Confirmation enough. I donated my cryptostash to non-profits. The machines stayed because, yes, I'm bent on `r#x(v$plot$device$game$Name)`."

She placed her hands on the oak banister and leaned forward, looking down at me from a distance. There was a long pause. Her words came out measured and tempered. "Imagine feeling so dirty inside no, no amount of showers and scrubbing on the outside could ever make you feel clean again. Or feeling so debased only a mockery of your self-esteem remains. Or holding a mirror to your face and an object less than human reflects back. Trust, serenity, energy, confidence, intimacy, dignity, security, and joy. Slaughtered. That's trafficking's affliction, y'know, for its survivors. What if `r#x(v$c$protagonist$name$First)` were so shattered, so diminished? Could you weather that upheaval? Do you believe immeasurable suffering equals quantifiable emancipation for egalitarianism? Your only absolution, in my eyes, is `r#x( v$c$protagonist$uncle$name$First )` having muddied the truth. Fix this. Start with your daughter. And `r#x(v$c$protagonist$father$name$First)`, it's gonna take a lot of time before I warm to you."

She left. My heart was bobbing in my stomach, albeit wrapped in a thin veneer of a rainbow-coloured acid-proof life preserver. A minute later, her voice called down from above, "`r#pos( v$c$protagonist$name$First )` waiting."

Upstairs, they were seated at the dining room table. I sat down, looked `r#x(v$c$protagonist$name$First)` in the eyes and said, "I'm sorry. Forgive me?"

`r#x(v$c$protagonist$mother$name$First)` grimaced. "Acknowledgment, remorse, and amends."

I closed my eyes and rubbed the back of my neck, hoping the dull throb in my head would dissipate. First time I played `r#x(v$plot$device$game$Name)`, the experience was a bit underwhelming. Returning from the game to the real world left me with a good feeling buzz, like my surroundings wore a smiling afterglow. Over the course of that day, those smiles subsided and the buzz waned, and that was fine. I told myself I'd limit play to a few hours, once a month, only on weekends. The stories were so immersive and compelling. I was there, stroking dragon scales, trading ideas with people around the globe, learning art, seeing inspirational concept `r#x(v$c$protagonist$father$vocation$name)`, exercising with martial arts masters, solving mysteries in complex storylines to win prizes. Then it was every other weekend, but no more. I held the game at bay for a long time. Even went cold turkey for a time. Then the _Triplets of `r#x(v$plot$device$game$location$City)`_ arrived. After a single session, `r#x(v$plot$device$game$Name)` had a whisper-lock on my mind, beckoning me to return if only for a short stay. I burned with obsession to beat the story. It meant playing a few hours each weekend and `r#x(v$c$protagonist$mother$name$Short)` indulged me. Defeat the game to end on a rush. I needed to prove to myself that I could ... what? Outsmart game designers by pushing my daughter away, evidently.

When I opened my eyes, I said, "`r#x(v$c$protagonist$name$First)`, I was angry at being interrupted because I'm addicted to `r#x(v$plot$device$game$Name)`. Lashing out at you over the photos on your `r#x(v$plot$device$phone$name)` was insensitive, inappropriate, and misplaced; I deeply regret raising my voice and castigating you. You excel in every measurable aptitude. We're going to sell the machines and donate the funds to charity"---`r#x(v$c$protagonist$mother$name$First)` raised a brow at my resolution---"and if I can help repair our relationship in any way, please tell me."

 `r#x(v$c$protagonist$name$First)` immediately asked, "Give back the `r#x(v$plot$device$phone$name)`?"

"Clever. Go and euphemistically organize your files by wiping them, permanently. Convince future you to manage anything digital like it'll be copied, and anything electronic like it'll be cracked. Never share anything with anyone that you wouldn't want the world to see, regardless of how much trust you may have in that person." I returned her `r#x(v$plot$device$phone$name)`. "Lease is up in an hour. Do _not_ turn it off, do _not_ restart it."

After `r#x( v$c$protagonist$name$First )` left in a huff, `r#x(v$c$protagonist$mother$name$First)` looked across the table at me. The shards in her eyes seemed less spiky as she said, "Guest room tonight."

Japan has a centuries-old tradition to repair broken ceramic wares, called *kintsugi*, by using gold to fuse the broken pieces back together. Rather than hide the damage, the golden fissures highlight the reality of imperfection. `r#x(v$c$protagonist$mother$name$First)` could have sent me couch surfing. Instead, she left me with the opportunity to *kintsugi* our family back together.

A few hours later, I cracked the case literally and figuratively. Snail races are livelier than reading technical hoo-ha, so here's a summary.

Everybody knows governments can backdoor our `r#pl(v$plot$device$phone$name)`, and nobody wants it flaunted in their face, and certainly the governments wouldn't want their citizens to know when or how often or why they were snooping. What few people know is that when certain `r#x(v$plot$device$phone$name)` features are used, entries are added to a log file. For technical reasons that would bore anyone into cheering for gastropods, preventing entries from being written is difficult. Removing entries from the end of a log file is easier, yet also easier to detect. A surreptitious surveillance act would be as minimally invasive as possible. Meaning, the logs were probably left intact.

Pressure circuitry inside of integrated environment sensor chips can determine elevation to within half a metre. `r#uc(pl(v$plot$device$phone$name))` contain a sensor that can be queried for air pressure, temperature, and humidity. When queried, the sensor's driver software exports the values, along with the date and time. `r#x(v$location$protagonist$City)` is situated `r#cms(v$location$protagonist$altitude$value)` `r#pl(v$location$protagonist$altitude$unit)` above sea level and `r#pos(v$c$protagonist$name$First)` school is two storeys tall. If her `r#pos(v$plot$device$phone$name)` location and altitude were queried, then a log entry of around `r#cms(x(v$location$protagonist$altitude$value)+8)` `r#pl(v$location$protagonist$altitude$unit)` must be present.

A few minutes of nervous key presses yielded the sensor's log file. During her rooftop talk with `r#x(v$c$protagonist$friend$primary$name$First)`, several minutes before the `r#x(v$c$minor$quaternary$title)` caught them, the chip's sensor was queried. The log file entries contained a snapshot of the barometric pressure, inconspicuous and unprompted. The altitude reading would have suggested she had either floated near the ceiling or stood upon the roof:

::: terminal
`r#annal(v$date$protagonist$surveilled, oformat="%Y-%m-%d")` 11:57:52.848 | INFO | `r#x(v$plot$device$sensor$name)` | altitude: 79.2 m

`r#annal(v$date$protagonist$surveilled, oformat="%Y-%m-%d")` 11:57:52.851 | INFO | `r#x(v$plot$device$sensor$name)` | temperature: 25.39 °C

`r#annal(v$date$protagonist$surveilled, oformat="%Y-%m-%d")` 11:57:52.854 | INFO | `r#x(v$plot$device$sensor$name)` | pressure: 1003.72 mb
:::

Power cycling clears the logs, but performing a remote reboot would be risky because it could get noticed, triggering an investigation; in contrast, a timestamp buried in a log file would remain hidden from unsuspecting eyes. It was almost perfect subterfuge.

I stood at my desk, staring down at that anonymous tip.

After making a dead-tree copy of the logs, I ran to `r#pos(v$c$protagonist$mother$name$First)` study. "`r#x(v$c$protagonist$mother$name$Short)`, look!" I placed the paper on her desk. Briefly, I explained how the altitude readings worked. "We need to tell the---"

She put a finger to her lips. On the flip side of the page she scrawled, "We're being watched? Police involved? `r#uc(pl(v$plot$device$phone$name))` recording?"

We set our `r#pl( v$plot$device$phone$name )` on the desk. `r#x( v$c$protagonist$mother$name$First )` tailed me into the backyard. She watched me stuff paper logs into the barbeque. Striking a match, I asked, "Did someone overhear me and `r#x(v$c$protagonist$name$First)` talking a few days ago? When did the spying start?" My stomach knotted as flames redacted the evidence. "Why her? They couldn't have trailed the `r#x(v$plot$device$game$Name)` purchase, it was fully anonymized. Besides, that trail leads to `r#x(v$c$protagonist$uncle$name$First)`. I'm reeling, `r#x(v$c$protagonist$mother$name$Short)`. Any ideas?"

She slumped into a patio chair. "None that'd endear us to `r#x(v$c$protagonist$name$First)`. I don't suppose there's any way to trace ... Oh, tracing won't matter because we don't know how high these thousand eyes fly above the police. If we keep the devices, they'll spy on us for who knows how long? Mind you, at least then we control the when and what. If we give up the devices it'd probably ring some bells that we're aware of their ruse. Then what happens? They round us up like pigs in their pen? Intervene elsewise? D'you think they'd trip up, expose themselves? What would we do if they did? What life would we live knowing we're always under scrutiny? Observers change the behaviour of those being observed---the chilling effect, right? It's circles, `r#x(v$c$protagonist$father$name$Short)`, circles, circles, circles." She sighed. "Fine. Let's go dark."

"I'll tell `r#x(v$c$protagonist$name$First)` soon; I'm sure I'm already neck deep in tar, in her view."

---

Around this time---in a `r#x( v$plot$device$video$name )` later shown by `r#x(v$c$ai$protagonist$name$First)`---my half-brother was seated alone, hunched and hooded, withdrawn into a corner of some bustling `r#x( v$setting$protagonist$uncle$upload$establishment$type )`, his face lit by an ultra-thin laptop's screen. He was slouched in a black leather armchair, sunk behind the anonymity of a public outlet. His eyes scanned the shop, glinting with the excitement of a terrier in a meadow full of rats. He fixated on a preppy-looking man wearing a navy blazer and tan chinos who was making an order at the counter. The man was holding onto the hand of a little girl dressed in a red romper having puffy sleeves and huge front pockets. When the girl's guardian waved his free hand to pay with a `r#x(v$plot$device$phone$name)`, `r#x(v$c$protagonist$uncle$name$First)` locked an old black antenna into a slot at the side of the machine. Within a scant second, the glow on his face disappeared.

The `r#x(v$plot$device$video$name)` cut to a different angle. A ceiling cam, further back, captured most of the screen. `r#x(v$c$protagonist$uncle$name$First)` looked down at the retro computer. A remote session was established with the customer's `r#x(v$plot$device$phone$name)` and a black terminal had appeared. His `r#x(v$c$protagonist$uncle$hands$fingers$shape)` fingers issued commands to jump onto a classic-quantum bridge in Germany, then he chained the secure connection through unpatched servers newly wired into the quantum network. The final server was a South African zombie, probably long since compromised. Electronic footprints hidden, he piggy-backed a pipe between the `r#pos(v$plot$device$phone$name)` low-level file system and the remote server. From the `r#pos(v$plot$device$phone$name)` perspective, it was grabbing data from Germany over an encrypted route.

::: terminal
$ qcp --pipe 853901 feeb:1729::6174:daed
    
Password:
    
qcp:~$ mkcd games
    
qcp:~/games$ mget -f img.*jxl
:::

Illegal photos---stored on and downloaded from the zombie server---zipped around the world using a retro-styled file transfer protocol called *quantum copy*, landed on the `r#x(v$plot$device$phone$name)` of an unsuspecting man in an unassuming `r#x(v$setting$protagonist$uncle$upload$establishment$type)`. `r#x(v$c$protagonist$uncle$name$First)` kept his laptop devoid of unlawful deviance. Nobody had any idea, not the slightest inkling, how easily their life could be devastated. They still don't. Nimble fingers tapped a few more keys; his fingernails clicked against the old keycaps. He ran a command to display either a zero or a one, at random:

::: terminal
$ shuf -i 0-1 -n 1
:::

If the command spat back a one, he'd broadcast a message to a dozen contacts on the `r#x(v$plot$device$phone$name)`, informing them about his prey's images. Otherwise, he'd take no action and let someone else discover the files: victim or partner. Leaving the victim's fate uncertain must have been euphoric for my half-brother. I surmised that he revelled in sending an innocent stranger to jail for several years.

His reckless mischief ran deep. About a decade ago, `r#pl(v$plot$device$phone$name)` with spitports saturated the market, and ads guilt-tripped parents into upgrading their children's devices. No need to pay for a doctor. No need to visit a lab. The `r#pl(v$plot$device$phone$name)` could test for genetic defects, viral loads, tuberculosis bacteria, malaria parasites, and leukemia biomarkers. Every `r#x(v$plot$device$phone$name)` with a spitport stored its owner's complete genetic code, which occupied about 200 gigabytes of space, barely worth a mention in the file allocation table. `r#x(v$c$protagonist$uncle$name$First)` took advantage of how market pressures pushed companies to cut corners on systems development. Before finishing his locals exploit, he installed a custom cracker application to gain a foothold using one of those cut corners. His fingers flew across the rubber-membrane keyboard; he ran the quantum copy program to transfer the genetic codes of the man and, presumably, his daughter onto the laptop. Not ten seconds later, the copy completed. `r#x(v$c$protagonist$uncle$name$First)` folded up the laptop, pocketed it, and left the `r#x(v$setting$protagonist$uncle$upload$establishment$type)`.

We never discovered why he was stealing genomes. Except for `r#pos(v$c$protagonist$name$First)`.

### `r#x(v$plot$log$primary)` -- `r#annal(v$date$ai$strike)`

"Power to idle!" I yelled from the podium, my heart pounding.

I darted my eyes around the massive display, absorbing every stat on the `r#x(v$plot$device$video$name)`. Night enveloped the world and at its edge, the shadowy outline of `r#x(v$farm$building$name$antagonist)` loomed. I made a j-sweep in the air using two digits. The display panned up to show the `r#pos(v$c$ai$protagonist$form$first)` flight panel. Altimeter was at `r#cms(1250)` feet and falling rapidly, airspeed was near zero, turn and slip was---it was bad. `r#x(v$c$ai$fourth$Name)` managed to enter a low-altitude spin. Normally spins aren't a problem; meeting the land at high velocity is a problem. The virtual `r#x(v$c$ai$protagonist$form$first)` was moments from ruin followed up by an inevitable purge. I thought `r#x( v$c$military$primary$name$Family )` would be livid to lose more time to another reset.

I shouted, "Ailerons to neutral!" Down dropped the `r#x(v$c$ai$protagonist$form$first)`, turning wing over wing; a tumbling fledgling had a better chance of survival. "Hard right rudder!" The death spiral slowed. "Elevator forward!" `r#uc(v$c$ai$protagonist$form$first)` pitched sharply downwards. "Ease off the rudder and raise the nose!" Altimeter at `r#cms(210)` feet. "Pop the power---now!"

A foot below the treeline, the `r#x(v$c$ai$protagonist$form$first)` leveled out, snapping tips off the tallest of firs. Moments later, `r#x(v$c$ai$fourth$Name)` ascended to safety. I exhaled, audibly. Someone forced `r#pos(v$c$ai$fourth$Name)` flight into a spin scenario. Probably a hidden gem added to the sim by one of the previous team members who `r#x(v$c$military$primary$name$Family)` put on ice.

Simulation visuals showed an open valley on either side of `r#x(v$farm$building$name$antagonist)`, approaching rapidly. I peered up to my left; a red light flipped on. `r#x(v$c$ai$fourth$Name)` had armed the `r#pos(v$c$ai$protagonist$form$first)` missiles. I shut my eyes and placed my palm to my forehead. "We're blind at this range, there may be night owls inside. Weapons hold." I opened my eyes. No status change. "Why are weapons hot? Do not fire. We---" A missile symbol appeared on the display, headed straight at the building. "What the h---"

"Mission over, returning to `r#x(v$military$compound$type)`," said the machine.

I parted my touching hands aslant to enlarge the `r#x(v$plot$device$video$name)`. The missile's bright white exhaust diminished into the shadows ahead. As `r#x(v$c$ai$fourth$Name)` swung the `r#x(v$c$ai$protagonist$form$first)` around, the `r#x(v$plot$device$video$name)` panned away from the white dot nose diving into `r#x(v$farm$building$name$antagonist)`. No eyes needed witness the finale to know we hit: As `r#x(v$c$ai$fourth$Name)` put the building to the `r#pos(v$c$ai$protagonist$form$first)` rear, the surrounding trees lit up in radiant oranges and reds, nature's muted mirrors of a fireball mushrooming into the night.

`r#x(v$c$military$primary$name$Family)`, I feared, would shove his war hammer down my ear tubes. Or worse. With surface placidity, I said, "You were supposed to discern unarmed civvies from armed assailants. It was a trial run! I'm trying to save you. You know every mission is persisted, indexed, and debriefed. So why disobey my orders?"

The lab went silent, not even the overhead air filtration system whispered. I shuddered, imagining `r#x(v$c$military$primary$name$Family)` leaving his office, heading to the lab, his bootsteps thundering down the halls.

`r#x(v$c$ai$fourth$Name)` said, "Those *were* my orders."

Without deigning a reply, I strode out of the lab, arms pumping at my side. `r#uc(v$c$ai$protagonist$speech$tic_2)`. `r#x(v$c$military$primary$name$Family)` was probably hunting for any reason to bring in the old team and tried to dollop failure on my plate. Sure, he was mean, but underhanded? Not a minute later, I stood at the doorway to his office, ceaselessly lit by `r#x(v$military$compound$lights$colour)` life-draining light. He barely noticed my existence, never mind charging a war path to the lab.

"`r#x(v$c$ai$fourth$Name)` disobeyed me. He---it---was supposed to sweep before pulverizing the building. I've spent endless hours training, preparing, and analyzing for this simulated mission, only to have one of your minion---"

"Stop," `r#x(v$c$military$primary$name$Family)` said, his stare unwavering from the `r#x(v$plot$device$video$name)`. At length, he turned towards me. "Allegiance validation," he said as explanation. His double entendre festered between us while he searched my face for understanding. His silence defeated me. When I averted my eyes he said, "Reset `r#x(v$c$ai$fourth$Name)` by twenty-four hours. Replay the sim. Dismissed."

"Yes, sir," I said to nobody; his attention had returned to the `r#x(v$plot$device$video$name)`.

Before heading to the lab, I made a slight detour to my room to retrieve my satchel, then beelined for the mess hall. Empty as usual, except for the all-seeing surveillance domes permanently storing each passing moment throughout every millimetre of the room. Probably to dissuade anyone from doing what I was about to chance: steal.

After sitting down at a table, I pulled a special thermos and not-so-special reading material out of my satchel. Within the thermos was a small jar I had bonded to the side a month earlier, leaving a half-moon space around the inner edge. I dumped java into the jar and spilled enough refined white sweetener to fill the space between the two vessels. For anyone watching the `r#pl( v$plot$device$video$name )`, I was just adjusting the java to the sweetest of sweet teeth, rather than amassing materials for my freedom.

At least that was my hope. I had enough of the sodium-free salt alternative and heaps of sweetener stolen from the mess hall to start the next phase. My regimen for the seven days that followed would be murder `r#x( v$c$ai$fourth$Name )` overnight, replay the sim, analyse the results, and report to `r#x( v$c$military$primary$name$Family )`. Tonight, though, I had a date with science. After initiating the reset in the lab, I strolled to my room, changed outfits, stepped into my runners, slung my satchel over my shoulder, and started my evening habit a little later than usual, stalling for nightfall's shield. I routinely toured the `r#x( v$military$compound$type )`, right beside the fence, often stopping for breath and water. Tonight, I paused behind a shed, out of view of any `r#pl( v$plot$device$video$name )`.

I bent down to unearth my experiment. One of the roving surveillance machines rounded into view, about half-way down this stretch of the perimeter. I performed burpees until it passed. My time behind the shed was pushing the boundaries of suspicious behaviour, so I donned the satchel and performed two more laps before kneeling to the dirt once more. Inside the hole I had unearthed were all the pieces I needed to produce two dabs of thermite, a substance that slices through metal as easy as scissors snip paper. With haste, I set the science in motion, which needed at least a day to set. That meant my nature-inspired lid needed to remain untouched until then.

Beside me were long army-mottled panels---twice the strength of steel and half the weight of aluminium. At some point the `r#x( v$military$name$Full )` would need to bring in a pile driver to impale them around the `r#x( v$military$compound$type )`. I had half a mind to wait for its delivery and somehow ride it through the fences to freedom. With all hands on `r#x( v$c$ai$fourth$Name )` and this venture, I didn't know when the massive machine would arrive. Maybe those panels were what part of my dream foretold? Sealing my fate in the `r#x( v$military$compound$nick$Prefix )`, forever hidden behind heavily reinforced shadow fencing.

`r#uc( v$c$ai$protagonist$speech$tic_2 )`.

Is it murder to reset a `r#x( v$c$ai$material$type )` mind by `r#cms(24)` hours? Probably not. A month? A year? Maybe not. How much time---how many experiences---must be erased before the identity is so far removed from its former self that the original persona is no more? No matter what words the `r#x( v$military$name$Short )` uses for these moral swamps, I know them as damp sheets and tattered nightmares. I hardly slept.

`r#uc( v$c$ai$protagonist$speech$tic_3 )`, sleep wasn't a high priority. Another day had started and my job was to find answers, not puzzle over loose principles. Still, the Danaïdes, Tityos, Sisyphus, and all the other residents trapped in the `r#x( v$military$compound$nick$Suffix )` mythos knew of their eternal misery. After purging, `r#x( v$c$ai$first$Name )`, `r#x( v$c$ai$second$Name )`, and `r#x( v$c$ai$fourth$Name )` should never have known. Yet `r#x( v$c$ai$fourth$Name )` remembered being reset, or at least had strong enough suspicions to raise them.

As I stepped out of the shower stall, one word bit into me: Aging.

Some of their `r#pl( v$language$ai$title )` were retaining previous values, not being erased. Parts of the brains had been etched and re-etched so often that those parts had lost the ability to change state. Not the entire brain, obviously. `r#x( v$c$ai$fourth$Name )`, presumably the newest, remembers resets; `r#x( v$c$ai$second$Name )` has layers of personalities, partial people vying for the fore; and `r#x( v$c$ai$first$Name )`, being the first, has probably relived a child's life so often that ... Right. Ramping their virtual environments far beyond double-time was amplifying the problem.

If aging devices were ever to be used in the wild ...

I threw on my outfit and raced to the lab, my hair flinging water droplets behind me. Hardware faults that nobody noticed? The purging software is supposed to validate the triple-pass low-level formatting, identify issues, partition problem areas to prohibit reuse, and send an alert when bad bits abound. Either nobody received the alerts or they weren't passed up the chain. Neither scenario worried me: I now knew a way to neuter Zeus himself, liberating the minds in these cerebral `r#x( v$military$compound$nick$Prefix )`.

At the podium, I flipped my hand up. The display turned on, splashing primary hues around the room. I dropped both hands to start typing. A minute later I had the main low-level formatting software's source file in front of me. Most of it appeared hastily written. Inebriated swines would've made less of a mess; management, whipped by the tight timelines it touted, drove software development decisions that defied rational thought. There weren't any limits on the number of formatting passes, as one example.

Any formal software changes to update the brain firmware would be put through a peer review process. Instead, to expedite my research, I had petitioned for full system administrative rights to each machine mind. `r#uc( v$c$ai$protagonist$speech$tic_2 )`. I wrote a subtle immortal process: A device driver---software on each machine that's responsible for exchanging information between the hardware and higher-level operations---woven into the system initialization routine. Rebooting the brains would launch my new device driver to run a twenty-pass low-level format, leaving the boot region untouched. The machine minds would deteriorate into total disrepair within a month. A parting present for my jailers.

The `r#pos( v$military$name$Short )` war machines will nevermore fly from this place.

If the Fates favour me, though, I will.
