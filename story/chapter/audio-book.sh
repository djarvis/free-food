#!/usr/bin/env bash

# Invokes KeenWrite to convert each Rmd file to XML.
function rmd_to_xml() {
  for i in *Rmd; do
    java \
      --add-opens=javafx.controls/javafx.scene.control=ALL-UNNAMED \
      --add-opens=javafx.controls/javafx.scene.control.skin=ALL-UNNAMED \
      --add-opens=javafx.graphics/javafx.scene.text=ALL-UNNAMED \
      --add-opens=javafx.graphics/com.sun.javafx.css=ALL-UNNAMED \
      --add-opens=javafx.graphics/com.sun.javafx.scene=ALL-UNNAMED \
      --add-opens=javafx.graphics/com.sun.javafx.text=ALL-UNNAMED \
      --add-exports=javafx.base/com.sun.javafx.event=ALL-UNNAMED \
      --add-exports=javafx.graphics/com.sun.javafx.application=ALL-UNNAMED \
      --add-exports=javafx.graphics/com.sun.javafx.geom=ALL-UNNAMED \
      --add-exports=javafx.graphics/com.sun.javafx.text=ALL-UNNAMED \
      --add-exports=javafx.graphics/com.sun.javafx.scene=ALL-UNNAMED \
      --add-exports=javafx.graphics/com.sun.javafx.scene.text=ALL-UNNAMED \
      --add-exports=javafx.graphics/com.sun.javafx.scene.traversal=ALL-UNNAMED \
      -jar ../keenwrite.jar \
        -v ../variables.yaml \
        --r-dir=../bin \
        --r-script=../bin/editor.R \
        --image-dir=../images \
        -i $i \
        -o $i.xml

    cat entities.xml $i.xml > ${i%.*}.xml
    rm $i.xml
    echo $i
  done
}

# Invokes xsltproc to convert each XML file to text.
function xml_to_txt() {
  chapter=1
  for i in ??.xml; do
    xsltproc --stringparam chapter "$chapter" split.xsl $i > ${i%.*}.txt
    let chapter="$chapter+1"
  done
}

# Invokes POSIX commands to create separate text files for first 12 chapters.
function split_text() {
  NEW=
  for i in $(seq -f "%02g" 1 12); do
    grep -n "^By " $i.txt | \
      cut -f1,2 -d: | \
      cut -f1,2 -d' ' | \
      while read -r line ; do
        START=$(echo $line | cut -f1 -d:)
        OLD=$NEW
        NEW=$(echo $line | \
          cut -f2 -d' ' | \
          tr '[A-Z]' '[a-z]' | \
          iconv -f utf8 -t ascii//TRANSLIT)

        if [ ! $START -eq 5 ]; then
          COUNT=$(wc -l < $i.txt)
          S_SPLIT=$(($START - 1))
          E_SPLIT=$(($COUNT - $START + 1))
          head -n $S_SPLIT $i.txt > $i-$OLD.txt
          tail -n $E_SPLIT $i.txt > $i-$NEW.txt

          sed -i 's/^By /Part One, By /' $i-$OLD.txt
          sed -i 's/^By /Part Two, By /' $i-$NEW.txt
        fi
      done
  done
}

# Install Tortoise TTS (mrq):
#
# git clone https://git.ecker.tech/mrq/tortoise-tts
# cd tortoise-tts
# python3 -m venv tts
# source tts/bin/activate
# pip3 install -U scipy
# pip3 install -U psutil
# pip3 install -r requirements.txt
# sudo python3 setup.py install
function txt_to_tts() {
  pushd $HOME/archives/tts

  for i in $HOME/dev/writing/free-food/story/chapter/??-*.txt; do
    F=$(basename $i);
    VOICE=$(echo ${F%.*} | cut -c 4-);

    if [ "$VOICE" = "cassandra" ]; then
      CHAPTER=$(echo ${F%.*} | cut -c -2);
      D=$(dirname $i);
      #CLIPS="$D/../audio/$CHAPTER-$VOICE";
      CLIPS="/tmp/audio/$CHAPTER-$VOICE";
      mkdir -p $CLIPS;

      echo "./scripts/tortoise_tts.py --cvvp-amount 0.5 -p high_quality -O $CLIPS -v $VOICE < $i;"
    fi
  done

  popd
}

rmd_to_xml
xml_to_txt

# split_text
# txt_to_tts

