# Glossary

A glossary of words in or relevant to the novel.

## Intelligence

Terms relating to machine intelligence.

AI
: Artificial intelligence is the simulation of human intelligence processes by machines, especially computer systems.

AGI
: Artificial General Intelligence. An AI that can perform many tasks at or above human ability.

ANI
: Artificial Narrow Intelligence. An AI that can perform a single task near or above human ability.

ASI
: Artificial Super Intelligence. An AI that can perform most tasks exceeding human ability.

{{document.title}}
: Feminine form of autónomo, from the Ancient Greek word αὐτόνομος (autónomos), meaning, "having its own laws."

sentient machine
: A self-aware AGI or ASI that exhibits or possesses emotions.

sophont
: A self-aware AGI or ASI that neither exhibits nor possesses emotions.

## Definitions

Existing terms.

ambergris
: A solid, waxy, dull grey or blackish substance produced in a sperm whale's digestive system. Highly valued by perfume makers.

AR
: In *Morse Code*, a demarcation for end of transmission. In *Human Computer Interactions*, an acronym for augmented reality.

ATV
: An all-terrain vehicle designed for off-road travel over diverse terrains, including rivers, rocks, mud, and snow.

barycentre
: Common center of mass for multiple orbiting bodies.

bathtub curve
: A graph used in reliability engineering to predict when components will fail.

biopolymer
: A compostable, biodegradable organic-based plastic.

burlap
: A strong, rough brown cloth used for making sacks. 

CAD
: Computer-aided design.

calve
: When an iceberg or glacier sheds a smaller chunk.

CDC
: Centre for Disease Control. An organization responsible for controlling the introduction and spread of infectious diseases.

chromatophore
: A specialized pigment cell that can change colour by expanding or contracting.

ETA
: Estimated Time of Arrival.

ferine
: Wild and menacing.

honeypot
: A security tool that deceives attackers with fake valuable resources on a monitored and isolated network so as to detect and block unauthorized access.

linckiacyanin
: A blue copper protein found in sugar beet roots.

memristor
: The fourth fundamental circuit element, alongside resistors, capacitors, and inductors. Memristors can maintain a value (e.g., 1 or 0) without using electricity, making them a neuron analog suitable for neuromorphic computer architectures (i.e., brains).

mutex
: Mutually exclusive (used in software development).

port scanner
: Software that attempts to determine what programs may be running on a remote computer system. Sometimes used to help find exploits against vulnerable systems.

pterobilin
: A blue pigment found in feathers.

red seal
: An official certification, indicating a professional's formal endorsement.

slipshod
: Careless, untidy, or slovenly (especially about work).

telemetry
: Recording and transmission of data, such as location and elevation, from a remote system to a different location, often for monitoring and analysis.

transceiver
: A device that can transmit and receive radio signals.

vanned
: When a person is captured by a government agency and thrown into an unmarked van.

wet-bulb temperature
: A temperature measure that accounts for humidity. In humans, when the air is too hot and humid, evaporative cooling can no longer maintain our core temperature of 37 °C. A sustained wet-bulb temperature over 35 °C may be fatal.

## Fictive

Future technologies.

{{plot.device.enhancer.name.short}}
: An {{plot.device.enhancer.name.full}} is a contact lens that overlays a computer-controlled display onto the retina. Presents the wearer with improved eyesight (macro-, micro-, and telescopic) and increases the visible spectrum into the infrared and ultraviolet.

biomachines
: A device with biological and abiological components.

blacktop
: A car, often a taxi, covered in dark solar panels.

{{plot.device.sensor.name}}
: An integrated circuit (i.e., computer chip) that measures barometric pressure with high accuracy.

{{plot.device.radio.name}}
: A personal transceiver worn around the wrist.

{{plot.device.biotech.stimulant.full}}
: A {{plot.device.biotech.stimulant.short}} (also *{{plot.device.biotech.stimulant.slang}}* and *stimmer*) is a highly addictive device to stimulate the brain into releasing endorphins and oxytocins.

{{plot.device.phone.name}}
: A cellular-based personal communication and computing device that can project images, analyse DNA, translate, provide location services, and more.

{{plot.device.courier.name}}
: A wheeled, autonomous robotic courier that delivers parcels or carries luggage.

electrovert
: A vehicle having an internal combustion engine that was converted to battery electric.

{{plot.device.biology.modification}}
: A genetic modification that alters a person's body, usually before birth.

holorama
: A holographic drama played in 3D movie theatres.

{{plot.medicine.aging.pill}}
: A pill that slows the aging process.

laserblade
: A high-intensity laser beam capable of cutting.

nanoslice
: An extremely small portion.

nanotendon
: A microscopic fiber having immense tensile strength and wrapped with muscle analogs that can rapidly stretch or contract.

neuromorphing
: Hardware capable of rewiring itself by re-routing and re-etching electrical synapses. Deeper etches create stronger bonds, allowing for persisted memories.

neuropunk
: A form of punk rock music that is played directly into the brain, using similar technology as a {{plot.device.biotech.stimulant.short}}, without addictive properties.

{{plot.money.crypto.short}}
: See *{{plot.money.crypto.name}}*.

{{plot.money.crypto.name}}
: An untraceable digital currency used globally.

{{plot.device.camera.name.short}}
: A {{plot.device.camera.name.full}} is a palm-sized flying drone used to record audio and video of its operator.

perception lattice
: A 3D matrix of interconnected metal wafers that form a brain analog capable of perceiving the world.

policenet
: A computer network for transferring highly sensitive data regarding police operations and prisoners.

{{plot.device.game.Name}}
: An immersive virtual reality system allowing its users to interact with a simulated world using touch and thought.

{{plot.device.network.browser.name.short}}
: A highly secure browser that uses multi-layered quantum-safe encryption to connect to the {{plot.device.network.advanced.name.full}}.

roadnet
: A network of publicly accessible street cameras that display traffic conditions.

{{plot.device.deodand.name.full}}
: An autonomous robotic honeybee for pollenating plants.

robopollinator
: See *{{plot.device.deodand.name.full}}*.

satnet
: A satellite network in orbit around Earth.

spitport
: A {{plot.device.phone.name}}'s female connector that receives a stick containing DNA to analyse, typically a mouth swab.

{{plot.device.vehicle.child.name}}
: An autonomous, electric vehicle for transporting up to two children within a city or rural area.

transing
: A way of sending thoughts between people using a computer and special headgear that reads brain signals.

{{plot.device.video.name}}
: An abbreviation for *video*, or a video recording.

{{plot.device.display}}
: A display system on or as a wall, usually permanently affixed.

{{plot.device.flashlight.name}}
: A palm-sized flying flashlight that outputs an extremely bright flood beam (200,000 lumens).

VoQN
: A protocol for relaying voice over a quantum network.

{{plot.device.software.deliberation}}
: A decentralized peer-to-peer program capable of video conferencing, texting, and file sharing. No government controls it.

## Expressions

Phrases spoken by characters.

{{c.protagonist.mother.speech.tic_2}}
: Absolutely.

BH
: Black hole.

crocodiled
: Thoughts that have preoccupied one's mind (as in "something's been eating at me," though with great voracity).

jellyfishing
: Being spineless, not standing up for one's self.

gimme the deep
: Asking for a deeper meaning.

gen ten
: Tenth generation.

go dark
: To stop using devices that are connected to the Internet or {{plot.device.network.advanced.name.full}}, pay for items using cash, and leave as little digital trail as possible.

no lip
: No lip service, as in, don't talk back.

pack or stack
: Leave (i.e., pack your bags) or stay and help (e.g., stack items).

raging
: All the rage, as in, cool.

rate busting
: Software designed to slow down responses to external systems making too many automated requests for data too quickly.

scorch
: The blistering period of the day when the sun’s intense heat renders outdoor conditions unbearable.

skep
: Skeptical.

synesify
: A synesthesia experience.

synesthesia
: A transformation of perceptions where stimuli are interpreted as another modality.
