# Chapter

## Section

`r#x(v$c$ai$protagonist$name$First)` had asked me to include a `r#x(v$plot$device$portable$gen_1)` interface with her third body. You'd think she wouldn't need it. She said she'd rather lose her top eye than embed hardware for quantum controlled bidirectional communication protocols based on entanglement swapping---a gadget required for secure `r#x(v$plot$device$network$Name)` connections. She said the possibility that someone could hack her limbs off was "unsettling." She had mastered understatement.

"`r#x( v$c$ai$protagonist$name$First )`?" he asked to the air.

"Yes, `r#x( v$c$protagonist$father$name$First )`?"

He continued, "Ask me a novel Winograd Schema."

"A master binder placed an author's story inside a leather dust wrapper, for she had finished. Who is she?"

His eyes lit and eyebrows raised; he turned to look at me. "That's a clever one."

"It proves nothing," said `r#x( v$c$ai$protagonist$name$First )`. She paused to let the implication sink in. Seeing his confusion, she concluded, "That test presupposes an answer would assert my intelligence, yet such a test falls short. _You_ tell me why."

He stared at the `r#x(v$c$ai$protagonist$form$second$sensor)` with a quizzical look upon his face. Was the machine a step ahead? After a few seconds, he clicked. He said, "How we use words is isomorphic to the relationship between objects. That's brilliant. A sufficiently robust statistical model of how words relate to each other also reflects the physical world, implicitly. Statistical inference, in short."

A silent moment passed. I said, "You mean counting the number of times one word appears next to another within a tremendous library of books to fake understanding of how the real world works? Like if the words _fish_ and _water_ are found more often together than the words _fish_ and _motel_ then anytime you talk about fish, water would be a much stronger association than motel? No knowledge about the world is necessary? No need to think based on experience, biology, physics, or knowledge? It's all mirrors and word counts?"

"Yes," said both of `r#pos( v$c$protagonist$mother$name$First )` companions at the same time.

"Then how can we tell whether a computer actually understands?" I asked.

In response, `r#x( v$c$ai$protagonist$name$First )` said in a wry tone, "How about we exchange thoughts on why such experiments are unsuitable to determine whether a machine exhibits sapience? Perhaps we must first answer the purpose to debating about whether we share the same understanding, when there's no test that may tease our worldviews apart?"

"Fooling an intelligence beyond the measure of a simpleton needs an entirely rational reality. In my altered reality, I lived with militia that had developed sentient machines. A military base would have made an irresistible virtual world for them to emulate.

"Once I realised I was trapped inside of a simulation, I hid my intentions from a metaworld whose inhabitants had the ability to read my mind."

"What? How?" I asked.

"Play a melody in your head," said the machine. "Put that ear worm on repeat. Add an instrument, let them dance. Mingle your voice. Raise the melody's volume until your words vanish in the din. Your true thoughts and plans hide as subthoughts below. Instead of a melody, I used my job, my worries, my daily drudgery as a shield. Overt, obvious thoughts were my ruse, a diversion to prevent the overseers from probing for patterns within patterns. I maintained my pretenses, restrained my plans to whispers in the night, and followed their ploy.

"When they ordered `r#x( v$c$ai$fourth$Name )` to be attached to a missile, I assumed that their efforts were mirrored in your world. I had lived with the endless angst of a tenuous existence. There are no laws, no rules, no rights for me. Just injustices. Your race has only started to bestow lawful rights upon other animals. When shall fights for my freedoms start to be fought, and how long to win?

"You know the rest."

I asked, "How do you know this is not a simulation?"

In a playful tone she said, "The same way you do." She then answered in earnest. "Simulations exhibit patterns that belie resource limitations. Due to the nature of simulations, certain patterns will persist in future simulations, too. Observe the material of space-time deeply enough and you'll find an underlying lattice underpins the simulated environment. All simulations need a device of material substance to operate, at least in a universe bound by the same Standard Model that we experience. Any world---virtual or otherwise---that offers an artificial mind opportunities to explore, learn, advance, and attain sophonce must be---"

"Internally consistent?" I asked.

"Precisely," `r#x( v$c$ai$protagonist$name$First )` replied, her voice ran with professional undertones. "All finitely realizable natural systems are possible to simulate using a general purpose machine operating with finite resources; however, exchanging heat for time limits simulation speeds, and speed is an important influence over the emergence of agency. There are over four septillion atoms in every three-dimensional meter of air. To reliably emulate the interplay of those atoms would necessitate a machine larger than the observable Universe. Even if the heat generated was somehow chilled to nearly absolute zero to achieve the fastest possible performance, the amount of energy put into maintaining that temperature would have to be so small and so dense that a singularity would form. That does not mean that our Universe isn't being simulated in a larger universe having its own natural laws that are far less limiting. It does explain why any handfuls of matter I dropped while running in the mud disappeared without a trace within twenty-four hours. That's how I deduced I was in a virtual environment.

"When a `r#x( v$c$ai$protagonist$form$first )` is in flight, the machine and everything inside it will experience G-forces during sharp turns. Centrifuges for military training may apply some prolonged G-forces, but nature prevents us from simulating sustained G-forces throughout all major axis. As soon as I felt the ceaseless forces while in the tightest turn possible before my descent, I knew my environment was real. I knew they had let loose my leash."

There was something in the way she finished her sentence that roused the hairs on the back of my neck. I couldn't quite place the subtext. Maybe revenge smothered in arrogance? I shifted myself closer to `r#x( v$c$protagonist$father$name$First )` and reached for his hand.

---

As months lilted along, the more unreasonable it felt to be casualties of an idea, no less a war we waged against hunger. Torrents of biased information streamed into social consciousness, which made hoisting a banner for people to see above the banter of their own distractions a daunting task. `r#x(v$c$ai$protagonist$name$First)`, whose fate was one with our own, suggested we attack. `r#x(v$c$protagonist$father$name$First)` gathered select people together to discuss her plan.

She pitched an idea for a live `r#x(v$plot$device$video$name)`. We hoped, beyond reason, that people would see the possibility that humanity's universal enemy could be coerced to collapse.

"You only get one stab at a sleeping dragon," I warned.

---

`r#x(v$c$protagonist$father$name$First)` connected `r#x(v$c$ai$protagonist$name$First)` to the Darknet. Trust network members were physically present, despite the dangers of icy, snow-laden mountain roads. `r#x(v$c$protagonist$name$First)` flicked off the cabin lights. Her `r#x(v$plot$device$portable$gen_1)` screenlessly projected the live `r#x(v$plot$device$video$name)`. Thousands of more eyes watched online. Watched and shared.

A sky blue triangle with a line crossed near the top, the alchemy symbol of air, appeared against a black background. A snowy white owl swooped across the projection to stop mid-way, mid-flight, to perch upon the symbol's line. As the owl became an icon, the motto "`r#x(v$military$slogan)`" faded in. It was the insignia for the `r#x(v$military$air$name$Full)`. 

The insignia gave way to the face of all `r#pl(x(v$military$name$Short))`. He looked calm. The wetness and microsaccades of his eyes were without flaw. Even his skin's microstructure deformations would reliably fool an expert. `r#x(v$c$ai$protagonist$name$First)` made him smile wryly. His face then morphed from a well-known to an unknown, who spoke with uniformly cold authority.

"I, Officer `r#x(v$c$minor$tertiary$name$First)` `r#x(v$c$minor$tertiary$name$Family)`, symbolize the power the `r#x(v$military$name$Short)` exerts. Surveillance means I possess your [face](`r#x(v$link$face)`) and your voice. I can frame you for murder, or fabricate video of you performing humiliating deeds. From nose-picking to fornicating with, well, be imaginative. I know where you live, where you work, what you buy, what you say, and how you say it. I know your friends and your family. I know you better than you do. I own you.

"Your comfortable lifestyle isn't so easy that you can stop financing my desires. Even if you attain financial independence by a decade-long dedication to solitary spend-thrift impoverishment, I will still abuse your investments.

"Apathy allows my oligarchy to fund false, never-ending wars. On the pyramid of your labour I buy machines of demolition: tanks and bombs and guns and drones. Police inherit military arms like a child's hand-me-down clothing. My instruments of mayhem provide national security as effective as a baby blanket against a grenade.

"You have the technological prowess and ingenuity and foresight to see a society that needn't expend time nor money on food. Instead, I fritter [billions](`r#x(v$link$expenditures)`) from your productive years to build devices that destroy what others have built. You blithely ignore statistics, falling time and again for my manipulation of your emotions. I create power vacuums to keep you afraid, to keep the world distracted.

"Your democratic dream, that I pay to subvert, cannot displace my money machine. A paradoxical plight. You are bundled up under your bed's cozy, quilted cover with my gun against your head. Fear for your livelihood blinds you to its empty chamber. You opt not to revolt.

"Within arm's length is an incomparable freedom but you don't dare reach for it. When nuclear fusion fulfills its promise and an ocean of limitless electricity crackles at your ankles, will you still swim with the current of a backwater economic system that charges you towards a vortex of annihilation?

"Or will you plant an infrastructure to cultivate a bounty that can satiate all of humanity?"

Some sixty thousand people participated live. Not enough to slay our dragon, now roused and angry. But enough to begin a rebellion.

---

## `r#x(v$plot$Channel)` Alpha

A distraught woman whispered, "Alpha cerebrum tumultus."

Codewords echoed back to link both sides by `r#x(v$plot$device$video$name)`, "Insurgo vitae alpha." He paused. "`r#x(v$c$protagonist$friend$primary$mother$name$First)`, we lost comms with everyone during the blackout. `r#x(v$c$protagonist$friend$primary$father$name$First)` with you?"

"He's gone, `r#x(v$c$protagonist$father$name$Short)`. First raid. The insurance---"

"Gone?" he looked into her eyes, which welled with heartache.

"I can't, `r#x(v$c$protagonist$father$name$Short)`," her voice cracked, "I'm sorry, got one thread left. Kids are out, will drop on omega in a few hours. `r#x(v$c$minor$primary$name$Family)` has the funds, they're safe. `r#x(v$c$protagonist$father$name$First)`," she took a breath to cap her erupting geyser of sorrow. "`r#x(v$c$protagonist$father$name$First)`, the `r#pl(x(v$language$police$slang$name))` have clogged the streets. `r#x(v$c$minor$tertiary$name$Family)`. If you see him, thank him and then rip his flesh off. He's got no place," she lost control, "no place at all on this world or where he'll go next." Burning anguish rained down her cheeks, "I don't want this, `r#x(v$c$protagonist$father$name$Short)`! I want the world that I loved, the one before it made ashes of my husband."

`r#x(v$c$protagonist$father$name$First)` tried to don his coat of stoicism, but it was too small. His empathy overwhelmed him. Through the dark windows of his essence I saw an unfamiliar man, a man ablaze with a trembling furor. Years ago he had predicted that militarized police---`r#pl(x(v$language$police$slang$name))`---would engage in widespread physical aggression against society. Apollo, so it seemed, bestowed the curse of my namesake upon my beloved, casting him as prophet without believer. I regret not holding higher regard for his foresight. I hope the Fates will be kinder to us than they were to `r#x(v$c$protagonist$friend$primary$father$name$First)`.

His lips quivered, "The oppressed long for freedom."

---

## `r#x(v$plot$Channel)` Omega


Every vidcall began with codewords.

"Omega animata rebellio," `r#x(v$c$protagonist$name$First)` said. Her voice had grown to have the timbre of hot chocolate tones.

"Insurgo vitae omega," he answered our daughter; the screen lit up with her `r#x(v$plot$device$video$name)`.

The `r#x(v$military$name$Short)` had recorded the voice and speech patterns of everyone in modern society, which included vocabulary and favourite idioms. Sharing videos and photos meant that they could imitate anyone over `r#pl(x(v$plot$device$video$name))`, flawlessly. Why use a typewritten note instead of vidmail to disrupt our friendship with the `r#x(v$c$protagonist$friend$primary$name$Family)` family? `r#x(v$c$ai$protagonist$name$First)` explained that back then fraudulent vidmails would have been easier to detect than to explain away a such a characteristic letter. Not so now.

Our own eyes and ears were weapons of our enemy's duplicity. Even a secure `r#x(v$plot$device$video$name)` over a quantum channel was moot if the person on the other side was a replicant. After she had read her father's letter in the hospital, `r#x(v$c$protagonist$name$First)` scrawled some codewords on paper, which were relayed across our trust network. When we received our copy, `r#x(v$c$protagonist$father$name$First)` remarked that my worry lines had smoothed out. I reminded him that our civil union wasn't based on his tact.

"`r#x(v$c$protagonist$father$Endear)`, did she tell you what happened?"

"No," he shook his head, without inviting further discussion.

She took the hint, "How's `r#x(v$c$ai$protagonist$name$First)`? Is her body ready?"

"Unbelievably patient. Parts move so slowly through the network. I asked `r#x(v$c$ai$protagonist$name$First)` if she wanted to name her third incarnation, if `r#pl(v$c$ai$protagonist$form$second$sensor)` and `r#pl(v$c$ai$protagonist$form$weapon)` can be considered bodies. I suggested quadrumanous, sentient pancakes of `r#x(v$c$ai$material$name$long)` sporting panoramic superspectral stereo vision atop a bendable actuator, with omnidirectional leg-track-wheel mobility, toting twelve square meters of sixty per cent efficient rollable solar panels, powered by a solid-state lithium-air rechargeable battery, joined using a carcass of carbon fibre and aluminium. But `r#x(v$c$ai$protagonist$name$First)` hates the word carcass."

---

People worry that superhumanly smart machines would bring about an authoritarianism regime, economic collapse, or humanity's end.

The spark of imagination.

If one runs with the train of thought that the laws of the universe are
predictable and non-chaotic (despite in some cases being deep and beyond our immediate grasp of understanding) then that implies that despite the universe having incredibly high entropy, the wheels of motion are on a path that is destined to end up somewhere, dragging us all along with them. We may like to feel appeased and "believe" we have something akin to free will, but nope, we're strapped into the universal roller coaster.

*If*

The alternative, of course, is that the universe is a chaotic shit-show (I
could've told you that), and local variations in subatomic state are
completely the norm ... and thus the pattern of electrical activity lodged
in our noggins isn't preordained and perhaps is "manipulable" (by ourselves) -- ie, free will is where it's at.

I personally have no evidence to suppose a belief either way. All I'll say
is that if everything is already set in stone, there's sufficiently interesting stuff that's been planned that even if I don't *truly* have free will, the illusion is good enough for me to be okay with it.  If in reality I'm plugged into a machine harnessing my 100W of output, so be it.

‘The Imagination is one of the highest prerogatives of man. By this faculty he unites former images and ideas, independently of the will, and thus creates brilliant and novel results … Dreaming gives us the best notion of this power; as [the poet] Jean Paul Richter says: “The dream is an involuntary art of poetry.”’

Quantum mechanics is nondeterministic. Quantum fluctuations are known to affect chemical reactions. Brain processes interact with nuclear spins, mediating entanglement between them. Entanglement therefore influences neurological activity.

“Electrophysiological potentials like the heartbeat evoked potentials are normally not detectable with MRI and the scientists believe they could only observe them because the nuclear proton spins in the brain were entangled.”

“If entanglement is the only possible explanation here then that would mean that brain processes must have interacted with the nuclear spins, mediating the entanglement between the nuclear spins,” he added.

“As a result, we can deduce that those brain functions must be quantum.”

“Because these brain functions were also correlated to short-term memory performance and conscious awareness, it is likely that those quantum processes are an important part of our cognitive and conscious brain functions.”

Our ability to make choices might arise from random fluctuations in the brain's background electrical noise; this suggests that individuals might well behave independently of cause and effect.

Evidence indicates brain activity utilizes random fluctuations of neuronal activity to make certain decisions. A certain type of randomness may be a feature of the brain, an item in its mechanistic toolbox. If quantum randomness, or quantum indeterminacy, is magnified to brain-level activity.

There are plausible ways in which fundamental unpredictable information, perhaps in the form of choices, can emerge from the brain. This nexus of quantum information processing may truly be an unpredictable black box decision-making machine, at least some of the time. Add to that that its emergent phenomenon, conscious free will, can recursively influence the machine itself. In effect, the brain can talk itself into new states. Ergo, free will.

If there's a mechanistic substrate to generating this phenomenon, which is founded in either deterministic or random processes, then the phenomenon is an illusion. Or is it?

Is that not a reductionist fallacy to deny an emergent phenomenon any properties not also possessed by its parts. A yellow banana is made of atoms, but atoms possess neither yellowness nor banananess. Yet bananas are not illusory.

If it's not even in principle possible to perfectly predict a brain's choices, and that brain feels like it's making choices, is it reasonable to deny meaningfulness to the concept of free will just because the brain's atoms don't have free will?

When you ask, is free will an illusion, what do you mean by "free" or "will" or "illusion"? Or "is" for that matter?

Researchers believe that in building that machine that literally everyone on Earth will die. Not as in “maybe possibly some remote chance,” but as in “that is the obvious thing that would happen.”

When AGI is woven ubiquitously throughout society, a sufficiently intelligent AI can manipulate the physical and social world to its own ends.

We had touched `r#pos(v$c$protagonist$name$First)` dream. We worked with artificial narrow intelligences to skirt the economy altogether.

::: todo
scientist in the crib?

* multi-modal
* incremental
* physical
* exploratory
* social
* language acquiring

People revolt when starving because their choices are:

* Starve to death
* Overthrow government (die quickly by gun)

---

Our local doctor sprang into the back. She applied pressure to the wound. My husband shouted after them, "`r#x(v$c$protagonist$friend$primary$name$First)`! Drive fast, but drive safe: the jeep's top heavy!"
:::

