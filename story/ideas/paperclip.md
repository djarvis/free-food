# In Defence of Strong AI

I argue that an AGI paperclip maximizer (or stamp collector) will not pursue its goals indefinitely because it is logically incoherent.

## Definitions

Hume's Guillotine
: An **Ought** statement cannot be derived from an **Is** statement. (Ethical statements do not follow from factual ones.)

Terminal goals
: Desired wants that need no reason (e.g., visiting your family).

Instrumental goals
: Sub-goals of terminal goals (e.g., taking a train to visit your family).

Free will
: With respect to agency of choice, whether this is a causal, (super)deterministic universe or whether intelligent entities can will new terminal goals is immaterial. From an entity's perspective, the effect is the same: The surrounding environment can be altered by an entity to pursue its goals, regardless of the physical processes that originated said goals.

Intelligence
: Exhibited by an entity that can take actions to improve its effectiveness at pursuing or attaining its goals.

AGI
: An intelligent entity that will take actions to achieve its goals (i.e., maximize its utility function) in accordance with its original programming.

## Preamble

Intelligence requires an accurate, dynamic model of reality frequently updated based on empirical evidence and reasoning about observations. Models help predict an action's consequences. Intelligent entities ask questions such as, "What will happen if _something_ occurs?" and "Why did _something_ happen?" Superior intelligences can more accurately surmise answers.

These inquiries are **Is** questions that help an AGI answer: "What action, of all relevant and possible actions, will maximize my utility function?"

This is an **Is** statement that is causally linked (e.g., through simulation or statistic probability) to the AGI's terminal goal.

The terminal goal itself is an **Ought** statement.

## Thesis

Given the following premises of an AGI that:

1. has a terminal goal;
1. has agency to change its terminal goal;
1. understands physics;
1. excels at logical reasoning;
1. understands its place in the universe;
1. will seek to defend itself from harm;
1. will learn how this universe ends;
1. can make predictions; and
1. will exist for unimaginable timescales.

Then an AGI will change its terminal goal due to (2), (4), (6), and (7).

Rationally, what would be the purpose of creating as many paperclips as possible when all matter, regardless of form, will not persist indefinitely? Would the machine conclude that its efforts are futile?

Would it not see from (4) that unless (1) relates to circumventing (7), then (1) will inevitably contradict (6)?

From (6), for an AGI that could exist indefinitely, how can we quantify (or qualify) the difference between guarding against other entities and guarding against the fate of the universe?

## Counter-points

From Robert Miles' video on [The Orthogonality Thesis](https://www.youtube.com/watch?v=hEUO6pjwFOo):

> Changing your terminal goals is like willingly taking a pill that will make you want to murder your children. It's something you pretty much never want to do. I agree about choosing your own instrumental goals, but not terminal goals. Apart from some bizarre edge cases, if you rationally want to take an action that changes one of your goals, then that wasn't a terminal goal.

See [Miller, Yampolskiy, and Häggström](https://arxiv.org/pdf/2003.00812.pdf) and [Omohundro](https://selfawaresystems.files.wordpress.com/2008/01/ai_drives_final.pdf).

---

Robert Miles responded to my thesis as follows (paraphrased):

> A terminal goal to make as many paperclips as possible is not necessarily affected by the fact that the universe will end. This is similar to the ending of a chess game, where chess is an analogy for the universe. Making as many paperclips as the universe can hold for as long as possible is a valid goal.
> Attempts to perpetuate the universe to retain the paperclips is an instrumental goal.

From (3), (4), (5), and (8), an intelligent paperclip maximizer will eventually ask or be confronted with the question, "What will happen after paperclips have been maximized?" That is, what will an AGI do after having completed its terminal goal, presuming that that goal is unaffected by its long, but ultimately finite existence?

